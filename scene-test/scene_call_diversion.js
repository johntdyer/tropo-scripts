//scene_call_diversion

var callee = "sip:vinaduan@1sip2sip.info";
var agent = "sip:hellen@sip2sip.info";


var askOptions = {
    choices : "Yes",
    bargein : false,
    attempts : 1,
    timeout : 10,
    mode : "speech",
    interdigitTimeout : 1,
    onChoice : function(){
            say("calling agent");
            transfer(agent);
            say("Byebye");
        },
    onBadChoice : function()
    {
        say("Byebye");
    }
}

var transferOptions = {
    timeout : 10,
    onBusy : function(){
        ask("The called party is busy. Would you want to call his agent?",askOptions);
    },
    onError : function(){
        ask("The called party is unavailable. Would you want to call his agent?",askOptions);
    },
    onTimeout : function(){
        ask("The called party is not answered. Would you want to call his agent?",askOptions);  
    }
}

say("will call" + callee);
transfer(callee,transferOptions);