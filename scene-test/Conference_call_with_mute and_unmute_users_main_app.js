//late_entry.caller
var groupID = conferenceID;
var muteType = type;
var callerID = address;


var conferenceUnmuteOptions = {
  mute : false,
  joinPrompt : true,
  leavePrompt : true,
}

var conferenceMuteOptions = {
  mute : true,
  joinPrompt : true,
  leavePrompt : true,
}

var callUnmuteOptions = {
  timeout : 20,
  onAnswer : function(){
    say("welcome to tropo meeting. ");
    log("###"+callerID+"will join in the meeting soon");
    conference(groupID,conferenceUnmuteOptions);
  }
}

var callMuteOptions = {
  timeout : 20,
  onAnswer : function(){
    say("welcome to tropo meeting.But you can not speak anything.");
    log("###"+callerID+"will join in the meeting soon");
    conference(groupID,conferenceMuteOptions);
  } 
}

if(muteType == "unmute"){
  call(callerID,callUnmuteOptions);
}
else{
  call(callerID,callMuteOptions);
}
