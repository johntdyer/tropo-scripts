//broadcast_call_main
//http://172.21.0.15:8080/gateway/sessions?action=create&token=4b6e6c466e646a564a784b516152764f464a5661676f58664d78695079754543776b6f545a764b7968664649&muteUsers=sip:fewxjrsz@172.21.99.140:58097&unmuteUsers=sip:ytscnrih@172.21.99.140:53187&conferenceID=1234
var groupID = conferenceID;
var muteGroup = muteUsers.split(",");
var unmuteGroup = unmuteUsers.split(",");


var gateway_ipaddress = "172.21.0.15";
var app_token = "7267527141744f546f65746364494c775a494b41556758544956475a79544f756a4f614f7476626778427746";
var unmute_url = "http://" + gateway_ipaddress + ":8080/gateway/sessions?action=create&token=" + app_token + "&conferenceID=" + groupID + "&type=unmute" + "&address=";
var mute_url = "http://" + gateway_ipaddress + ":8080/gateway/sessions?action=create&token=" + app_token + "&conferenceID=" + groupID + "&type=mute" + "&address=";

function callapp(url){
    log("### calling caller app ###");
        try{
          connection = new java.net.URL(url).openConnection();
          connection.setRequestMethod("GET");
          connection.setReadTimeout(10000);
          connection.setConnectTimeout(10000);
          code = connection.getResponseCode();
        }
        catch(e) {
          throw {message:("Socket Exception or Server Timeout"), code:0};
        }
        if(code < 200 || code > 299) {
          throw {message:("Received non-2XX response: " + code), code:code};
        }
        try {
          is = connection.getInputStream();
                //return new String(org.apache.commons.io.IOUtils.toString(is));
        }
        catch(e) {
          throw {message:("Failed to read server response"), code:0};
        }
        finally {
          try {if(is !== null)is.close();} catch (err){}
          log("##################the is is :"+is);
        }
}

for(var i =0; i < unmuteGroup.length; i++)
{
  callapp(unmute_url + unmuteGroup[i]);
}

for(var j =0; j < muteGroup.length; j++)
{
  callapp(mute_url + muteGroup[j]);
}

