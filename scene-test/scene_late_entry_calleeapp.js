//late_entry.callee
//http://172.21.0.13:8080/gateway/sessions?action=create&token=6d6e46426659414e46497754517075706751735749626f6273454c636152754c69696f6e5741464e4f54556d&address=sip:ltang@sip2sip.info&conferenceID=1234
var groupID = conferenceID;
var calleeID = address;
gateway_ipaddress ='172.21.0.15';
var callee_token = '62496166696b4e5553624b53576a584b5241647255474e4c6a42747378754754756e566b4a7079714b726b66';
var callee_url = "http://" + gateway_ipaddress + ":8080/gateway/sessions?action=create&token=" + callee_token + "&address=" + calleeID + "&conferenceID=" + groupID;

var conferenceOptions = {
    mute : false
}


function call_callee(){  
  try{
    connection = new java.net.URL(callee_url).openConnection();
    connection.setRequestMethod("GET");
    connection.setReadTimeout(10000);
    connection.setConnectTimeout(10000);
    code = connection.getResponseCode();
  }
  catch(e) {
    throw {message:("Socket Exception or Server Timeout"), code:0};
  }
  if(code < 200 || code > 299) {
    throw {message:("Received non-2XX response: " + code), code:code};
  }
  try {
    is = connection.getInputStream();
          //return new String(org.apache.commons.io.IOUtils.toString(is));
  }
  catch(e) {
    throw {message:("Failed to read server response"), code:0};
  }
  finally {
    try {if(is != null)is.close();} catch (err){}
    log("##################the is is :"+is);
  }
}


var callOptions = {
  allowSignals : "exit",
  timeout : 10,
  onAnswer : function(){
    say("welcome to tropo, you are the meeting inviter and you will be in the meeting soon.");
    log("###"+calleeID+"will join in the meeting soon");
    conference(groupID,conferenceOptions);
  },
    
  onTimeout : function(){
        log("###callee onTimeout###");
        wait(5000);
        call_callee();
    },
    
  onBusy : function(){
        log("###callee onBusy###");
        wait(10000);
        call_callee();
  },

  onError :function(){
        log("###callee onError###");
        wait(10000);
        call_callee();
  }
    
}

call(calleeID,callOptions);