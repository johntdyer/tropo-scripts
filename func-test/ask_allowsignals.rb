=begin
  *Launch the call by https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&phone=sip:xxx
  *the script will be interrupted when received http signal request
  *https://gw.serv.ip.addr/sessions/<session-id>/signals?action=signal&value=exit
=end
number = $phone
call number
ask "What's your four or five digit pin? Press pound when finished, \
     The Linux filesystem structure has evolved from the Unix file structure. \
     Unfortunately, the Unix file structure has been somewhat convoluted over \
     the years by different flavors of Unix. Nowadays it seems that no two Unix \
     or Linux systems follow the same filesystem structure. However, there are a \
     few common directory names that are used for common functions. Table 3-4 lists \
     some of the more common Linux virtual directory names.", {
  :choices => "[4-5 DIGITS]",
  :terminator => '#',
  :mode => "dtmf",
  :allowSignals => 'exit',
  :onChoice => lambda { |event|
    say "On Choice"
  },
  :onBadChoice => lambda { |event|
    say "On Bad Choice"
  },
  :onSignal => lambda { |event|
    say "Received signals, will be interrupted."
  }
}
