#Change the callee number accordingly
#Callee hangup after heard say second 
#'log 3rd', 'log 4th' and 'log 5th' will be printed out

call "sip:chxd99@172.21.99.111:45678", {
  :onAnswer => lambda { |event|
    newCall = event.value
    newCall.say "say first"
    log "log 1st"
    wait 5000 #hangup from the callee
    newCall.say "say second"
    log "log 2nd" #verify the log can be printed
    wait 5000
    newCall.ask "what's your favourite color?", { 
      :choices => "red,blue,green",
      :onChoice => lambda { |event|
        say "you said " + event.value
      }
    }
    log "log 3rd" #verify the log can be printed
    wait 5000
    newCall.conference "1234", { :terminator => "*", :playTones => true}
    log "log 4th" #verify the log can be printed
  }
}
log "log 5th" #verify the log can be printed
