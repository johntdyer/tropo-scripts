#curl -L -k -v -H "Content-type: application/json; charset=UTF-8" -X POST -d '{"muted": "true/false", "token":"TOKEN"}' https://gw.serv.ip.addr/conferences/1234/participants/<participant_id>
participant_id = $currentCall.id
log "participant-id is " + participant_id
conference "1234", {
  :mute => false
}
