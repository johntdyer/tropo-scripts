#sms_ask_any_inbound.rb
ask "", {
:choices => "[ANY]",
:terminator => '#',
:timeout => 15.0,
:mode => "dtmf",
:interdigitTimeout => 5 ,
:onChoice => lambda { |event|
log "On Choice"
},
:onBadChoice => lambda { |event|
log "On Bad Choice"
}
