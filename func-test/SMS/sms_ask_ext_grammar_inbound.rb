ask "", {
:choices => "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/ask_grammar.xml",
:terminator => '#',
:timeout => 15.0,
:interdigitTimeout => 5 ,
:onChoice => lambda { |event|
log "On Choice " + event.choice.interpretation
say "you said " + event.choice.interpretation
},
:onBadChoice => lambda { |event|
log "On Bad Choice"
say "On Bad Choice"
}
}
