#sms_ask_inbound.rb
ask " ", {
	:choices => "[4-5 DIGITS]",
		:terminator => '#',
		:timeout => 15.0,
		:mode => "dtmf",
		:interdigitTimeout => 5 ,
		:onChoice => lambda { |event|
			log "On Choice"
			say "your choice is "+ event.value
		},
		:onBadChoice => lambda { |event|
			log "On Bad Choice"
			say "you didn't give the correct choice"
		}
}
