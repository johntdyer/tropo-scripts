call $phone, {
:network => "SMS", 
#:callerID => $callerid
}
ask "", {
:allowSignals => "exit",
:choices => "[4-5 DIGITS]",
:terminator => '#',
:timeout => 15.0,
:interdigitTimeout => 5 ,
:onTimeout => lambda { |event|
say "On Timeout"
},
:onChoice => lambda { |event|
say "On Choice"
},
:onBadChoice => lambda { |event|
say "On Bad Choice"
},
:onSignal => lambda { |event|
say "On Signal:" + event.value
}
}
