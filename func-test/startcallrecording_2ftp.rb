def ask_color
  ask "What's your favorite color? Choose from red, blue or green.", {
    :bargein => false,
    :choices => "red, blue, green",
    :attempts => 3
  }
end

def ask_format
  result = ask "Which audio format do you want to record, press 1 for w a v, 2 for mp3, 3 for a u?", {
    :mode => "dtmf",
    :choices => "1, 2, 3"
  }
  return result.value
end

for i in 1..3
  case ask_format
  when '1'
    chose_format = "audio/wav"
    say "you choose wav format"
  when '2'
    chose_format = "audio/mp3"
    say "you choose mp3 format"
  when '3'
    chose_format = "audio/au"
    say "you choose a u format"
  else
    chose_format = "audio/wav"
    say "wrong choice, will use the default record format"
  end
  session_id = $currentCall.sessionId
  #ftp_url = "ftps://qa_tropo:qa_tropo@173.39.154.1:990/QA/recordings/" + session_id + chose_format.gsub("audio/", ".")
  ftp_url = "ftp://public:public@10.75.187.217/QA/recordings/" + session_id + chose_format.gsub("audio/", ".")
  startCallRecording ftp_url, { :format => chose_format }
  ask_color
  stopCallRecording
  wait 5000
  say "Your will hear your recorded file," + ftp_url
end

say "call recording has been stopped."
