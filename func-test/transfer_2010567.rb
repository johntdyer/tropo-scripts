#JIRA TROPO-1650, Evo 2010567
caller = "sip:ryqalinw@172.21.99.243:49319"
callee = "sip:gyxjhklc@172.21.99.244:53477"
call caller, {
    :allowSignals => "exit_call",
    :onAnswer => lambda { 
        transfer callee, {
            :allowSignals => "exit_transfer",
            :terminator => "*"
        } 
}
