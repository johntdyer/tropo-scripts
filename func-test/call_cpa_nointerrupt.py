##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
call(phone, {
		    "timeout":60,
		        "allowSignals":"#",
			    "machineDetection": {
			            "introduction": 'https://ccrma.stanford.edu/~jos/mp3/gtr-jazz.mp3',
				            },
					        "onAnswer": lambda event : say("dtmf input is " + str(event.value.input) + ", and CPA usertype is " + str(event.value.userType)),
						    "onSignal": lambda event : log("==========> the call is interrupted")
						    })
