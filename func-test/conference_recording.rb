#conference_recording.rb
conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no phone number given."
    else
      log "#"*10 + " #{x} join in."
    end

    conferenceOptions = {
      :terminator => "*",
      :joinPrompt => true,
      :leavePrompt => true
    }
    
    call x , {
      :timeout=>90,
      :onAnswer => lambda { |event|
        newCall = event.value
        session_id = $currentCall.sessionId
        newCall.startCallRecording "ftp://qa_tropo:qa_tropo@ftp.pek.voxeo.com/QA/recordings/" + session_id + ".wav" 
        newCall.conference conferenceID, conferenceOptions
        newCall.stopCallRecording
        newCall.wait(1000)
        newCall.say "you would hear the recording file"
        newCall.wait(2000)
        newCall.say "ftp://qa_tropo:qa_tropo@ftp.pek.voxeo.com/QA/recordings/" + session_id + ".wav"
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }