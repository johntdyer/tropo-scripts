<?php
function ringingFNC($event) {
  setTag("voxeolabs-javen");
  _log("It's ringing!!! [" . $event->value->calledID . "]");
}
function ringingFNC2($event) {
  setTag("tropo-jack");
  _log("It's ringing!!! [" . $event->value->calledID . "]");
}
function connectFNC($event) {
  setTag("cisco-jack"); //leg B
  //$event->value->setTag("cisco-jack"); //also leg B
  _log("It's ringing for transferee!!! [" . $event->value->calledID . "]");
  say("transfer new destination success!!");
}
function successFNC($event) {
  setTag("cisco-javen"); //leg A
  _log("It's end!!! [override tropo-javen]");
  say("transfer end, good-bye!");
}
function timeoutFNC($event) {
  setTag("cisco-jack-timeout"); //leg A
  _log("Time out to transfer the call!!");
}
function busyFNC($event) {
  setTag("cisco-jack-busy"); //leg A
  _log("Failed to transfer the call to a busy number!!");
}
function failFNC($event) {
  //callee offline or reject
  setTag("cisco-jack-fail"); //leg A
  _log("Failed to transfer the call to a bad number!!");
}
function errorFNC($event) {
  //Illegal phone number or SipUri for callee, like sip:����@10.140.254.131
  setTag("cisco-jack-error"); //leg A
  _log("Encounter an error to transfer the call!!");
}
function answerFNC($event) {
  setTag("tropo-javen");
  _log("It's ringing!!! [" . $event->value->calledID . "]");
  transfer("45693820@10.140.254.131:5066", array(
      "tag" => "voxeolabs-jack", //leg B
      "terminator"=> "*",
      "onRinging" => "ringingFNC2",
      "onConnect" => "connectFNC",
      "onSuccess" => "successFNC",
      "onTimeout" => "timeoutFNC",
      "onBusy"    => "busyFNC",
      "onCallFailure" => "failFNC",
      "onError"       => "errorFNC"
    ));
}
call("x-lite@10.140.254.131:5062", array(
  "tag" => "voxeo-javen",
  "onRinging" => "ringingFNC",
  "onAnswer"  => "answerFNC"
));
?>