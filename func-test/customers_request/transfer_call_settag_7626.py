def ring(event):
  #setTag("tropo-jack-ring") #leg A
  event.value.setTag("tropo-jack") #leg B
  log("It's ringing for transferee!!! [" + event.value.calledID + "]")
def connect(event):
  setTag("cisco-jack") #leg B
  #event.value.setTag("cisco-jack") #also leg B
  log("It's ringing for transferee!!! [" + event.value.calledID + "]")
  say("transfer new destination success!!")
def success(event):
  setTag("cisco-javen") #leg A
  log("It's end!!! [override tropo-javen]")
  say("transfer end, good-bye!")
def timeout(event):
  setTag("cisco-jack-timeout") #leg A
  log("Time out to transfer the call!!")
def busy(event):
  setTag("cisco-jack-busy") #leg A
  log("Failed to transfer the call to a busy number!!")
def fail(event):
  #callee offline or reject
  setTag("cisco-jack-fail") #leg A
  log("Failed to transfer the call to a bad number!!")
def error(event):
  #Illegal phone number or SipUri for callee, like sip:����@10.140.254.131
  setTag("cisco-jack-error") #leg A
  log("Encounter an error to transfer the call!!")
def answer(event):
  setTag("tropo-javen")
  log("It's ringing!!! [" + event.value.calledID + "]")
  transfer("23915640@10.140.254.131:5066", {
    "tag": "voxeolabs-jack", #leg B
    "terminator": "*",
    "onRinging": lambda event: ring(event),
    "onConnect": lambda event: connect(event),
    "onSuccess": lambda event: success(event),
    "onTimeout": lambda event: timeout(event),
    "onBusy" : lambda event: busy(event),
    "onCallFailure": lambda event: fail(event),
    "onError": lambda event: fail(event)
  })
call("x-lite@10.140.254.131:5062", {
  "tag": "voxeo-javen",
  "onRinging": lambda event: setTag("voxeolabs-javen"),
  "onAnswer": lambda event: answer(event)
})