call "x-lite@10.140.254.131:5062", {
  :tag => "voxeo-javen",
  :onRinging => lambda { |event|
    setTag "voxeolabs-javen"
    log "It's ringing!!! [" + event.value.calledID + "]"
  },
  :onAnswer => lambda { |event|
    setTag "tropo-javen"
    log "It's ringing!!! [" + event.value.calledID + "]"
    transfer "23915640@10.140.254.131:5066", {
      :tag => "voxeolabs-jack", #leg B
      :terminator => "*",
      :onRinging => lambda { |event|
        #setTag "tropo-jack-ring" #leg A
        event.value.setTag "tropo-jack" #leg B
        log "It's ringing for transferee!!! [" + event.value.calledID + "]"
      },
      :onConnect => lambda { |event|
        setTag "cisco-jack" #leg B
        #event.value.setTag "cisco-jack"  #also leg B
        log "It's ringing for transferee!!! [" + event.value.calledID + "]"
        say "transfer new destination success!!"
      },
      :onSuccess => lambda { |event|
        setTag "cisco-javen" #leg A
        log "It's end!!! [override tropo-javen]"
        say "transfer end, good-bye!"
      },
      :onTimeout => lambda { |event|
        setTag "cisco-jack-timeout"  #leg A
        log "Time out to transfer the call!!"
      },
      :onBusy => lambda { |event|
        setTag "cisco-jack-busy" #leg A
        log "Failed to transfer the call to a busy number!!"
      },
      :onCallFailure => lambda { |event|
        #callee offline or reject
        setTag "cisco-jack-fail"; #leg A
        log "Failed to transfer the call to a bad number!!"
      },
      :onError => lambda { |event| 
        #Illegal phone number or SipUri for callee, like sip:����@10.140.254.131
        setTag "cisco-jack-error" #leg A
        log "Encounter an error to transfer the call!!"
      }
    }
  }
}