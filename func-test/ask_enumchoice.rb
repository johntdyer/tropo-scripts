options = {
  :mode => "dtmf",
  :choices => "0,1,2,3,4,5,6,7,8,9",
}
for i in 1..3
  result = ask "Pick a number from 0 to 9", options
  if result.name == 'choice' 
    say "You pressed number" + result.value   
  else
    say "You did not press digit number."   
  end
  wait 2000
end
