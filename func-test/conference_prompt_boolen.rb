#conference_prompt_boolen.rb
=begin	

This script is to test conference joinPrompt and leavePrompt you should run this script four times as follow.
*case1: joinPrompt=true, leavePrompt=true;
*case2: joinPrompt=true, leavePrompt=false;
*case3: joinPrompt=false, leavePrompt=true;
*case4: joinPrompt=false, leavePrompt=false

NOTE:
Add <beepURL>http://127.0.0.1:8080/tropo/beep.wav</beepURL> as a child node to <tropo> in tropo.xml. 
If joinPrompt/leavePrompt is set to true, this default audio file would be used.

*join in conference via REST API - https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&participants=sip:chxd99@ip:port,sip:hellen@ip:port,sip:janey@ip:port,sip:vina&conference=1234
*exit conference https://gw.serv.ip.addr/sessions/<session-id>/signals?action=signal&value=exit
=end
 
conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no phone number given."
    else
      log "#"*10 + " #{x} join in."
    end

    conferenceOptions = {
      :terminator   => "*",
      :joinPrompt   => true,
      :leavePrompt  => false
    }
    
    call x , {
      :timeout=>90,
      :onAnswer => lambda { |event|
        newCall = event.value 
        newCall.conference conferenceID, conferenceOptions
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }