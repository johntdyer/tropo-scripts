result = ask "Which kind of call do you want to setup with the other, Transfer or Conference?", {
  :mode => "speech",
  :choices => "transfer, conference",
  :bargein => false
}
say "you chose " + result.value

transferOperations = {
  :terminator => "#"
}

conferenceOperations = {
  :joinPrompt => { 
    "value" => "welcome to join Tropo conference"
  },
  :terminator => "*"
}

if result.value === "transfer"
  transfer "sip:x-lite@10.140.254.60:5062", transferOperations  
else
  conference "hello", conferenceOperations  
end

