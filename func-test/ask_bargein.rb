def ask_case
  result = ask "Hello, this is an application to test bargein parameter. \
                Press 1 to test bargein for true in dtmf, press 2 to test  
                bargein for false in dtmf, press 3 to test bargein for true 
                in speech, press 4 to test bargein for false in speech", {
    :mode => "dtmf",
    :choices => "1, 2, 3, 4"
  }
  return result.value
end
ask_digits = "What's your four or five digit pin? Press pound when finished"
ask_words = "What's your favorite color?  Choose from red, blue or green."
for i in 1..4
  case ask_case 
  when '1'
    ask ask_digits, {
        :choices => "[4-5 DIGITS]",
        :terminator => '#',
        :bargein => true, 
        :mode => "dtmf",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
  when '2'
    ask ask_digits, {
        :choices => "[4-5 DIGITS]",
        :terminator => '#',
        :bargein => false, 
        :mode => "dtmf",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
    when '3'
    ask ask_words, {
        :choices => "red, green, blue",
        :terminator => '#',
        :bargein => true, 
        :mode => "speech",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
  when '4'
    ask ask_words, {
        :choices => "red, green, blue",
        :terminator => '#',
        :bargein => false, 
        :mode => "speech",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
  end
  wait 2000
end


