# This test is to check that dial the DTMF digits
# defined by 'postd' after the call has connected.
#
# Define the number to dial in the URL:
# https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&phone=sip:vinaduan@sip2sip.info
#
# Testing process:
# step1: launch the application using URL in browser;
# step2: answer the call.

call(phone + ';postd=123',{
    "timeout":20
})
