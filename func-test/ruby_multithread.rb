ruby_multithread.rb
=begin
Reproduce cases, 
case1: use threads.each { |t| t.join } 
case2: use wait(30000) 
case3: add "$appInstance.block(10000)" before wait(30000) 
case4: add "$appInstance.block(10000)" before newCall.conference 

Expected result: ruby thread should be realsed after call end.
=end

phonelist = [] 
phonelist[0] = "ryqalinw@172.21.99.234:49319" 
phonelist[1] = "ynbgeald@172.21.99.234:51175" 
threads = [] 

phonelist.each do |x| 
  #x = phone.to_s 
  conferenceOptions = { 
    :allowSignals => x, 
    :joinPrompt => "Hello, welcome to the conference.", 
    :leavePrompt => "You are leaving the conference, goodbye.", 
    :mute => false, 
    :playTones => true, 
    :terminator => "*", 
    :onChoice => lambda { |event| 
        say("Disconnecting") 
    } 
  } 
  threads << Thread.new do 
    call "sip:" + x , { 
      :allowSignals => x, 
      :onAnswer => lambda { |event| 
        newCall = event.value 
        newCall.conference("1234", conferenceOptions) 
        log "==========> " + x + " is kicked out." 
        newCall.hangup 
      } 
    } 
  end 
end 

log "==========> before waiting 30 sec" 
$appInstance.block(20000) 
wait(30000) 
log "==========> after waiting 30 sec"