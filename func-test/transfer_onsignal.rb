#the script will be interrupted when received http signal request
#launch the call via REST API
#https://gw.serv.ip.addr/sessions/<session-id>/signals?action=signal&value=*
session_id = $currentCall.sessionId
log "session_id is " + session_id
transfer "sip:hellen@10.140.254.24:5061", {
  :allowSignals => '#',
  :onConnect => lambda { |event|
    say "transfer success"
  },
  :onSignal => lambda { |event|
    #say "receive the signal"
    log "receive the signal"
  }
}
