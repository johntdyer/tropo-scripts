//get_header.js
if (currentCall.getHeader())
{
    //log("Your header value is " +currentCall.getHeader("Request URI"));
    //log("Your header value is " +currentCall.getHeader("From"));
    //log("Your header value is " +currentCall.getHeader("To"));
    //log("Your header value is " +currentCall.getHeader("Via"));
    //log("Your header value is " +currentCall.getHeader("Contact"));
    //log("Your header value is " +currentCall.getHeader("Call-ID"));
    //log("Your header value is " +currentCall.getHeader("CSeq"));
    //log("Your header value is " +currentCall.getHeader("Max-Forwards"));
    log("Your header value is " +currentCall.getHeader("Route"));
    //log("Your header value is " +currentCall.getHeader("Record-Route"));
    //log("Your header value is " +currentCall.getHeader("Supported"));
    wait(3000)
    reject();
}
else
{
    log("Your header value was not found");
}
/* SIP:
May 18 03:29:08.670 DEBUG  PRISM ////1/[] [udp/172.21.0.123/5060-t-4] #SIP#: (i)[INVITE sip:9996100041@172.21.0.122 SIP/2.0\r\nContent-Type: application/sdp\r\nTo: <sip:9996100041@172.21.0.122>\r\nVia: SIP/2.0/UDP 172.21.0.122:5060;branch=z9hG4bK1nkec7c5n8iy4;rport\r\nVia: SIP/2.0/UDP 172.21.99.213:50339;rport=50339;branch=z9hG4bKPj20ce327d12034bc4b8100eb18b2abce8\r\nAllow: SUBSCRIBE, NOTIFY, PRACK, INVITE, ACK, BYE, CANCEL, UPDATE, MESSAGE, REFER\r\nRecord-Route: <sip:172.21.0.122:5060;transport=udp;lr>\r\nCall-ID: a2f1b36ac283434690772af9af2e10f0\r\nUser-Agent: Blink 0.3.1 (Windows)\r\nFrom: Win7 <sip:82617034@172.21.99.213>;tag=b2873dc937fc4d278c11f6031c56dacd\r\nMax-Forwards: 69\r\nContact: <sip:82617034@172.21.99.213:50339>\r\nx-sid: 6f5fa7c4f62ecce935646b57ea086a4e\r\nCSeq: 25672 INVITE\r\nContent-Length: 543\r\nRoute: <sip:172.21.0.123;lr>\r\nSupported: 100rel, replaces, norefersub, gruu\r\n\r\nv=0\r\no=- 3577865364 3577865364 IN IP4 172.21.99.213\r\ns=Blink 0.3.1 (Windows)\r\nc=IN IP4 172.21.99.213\r\nt=0 0\r\nm=audio 50014 RTP/AVP 9 104 103 102 0 8 101\r\na=rtcp:50015\r\na=rtpmap:9 G722/8000\r\na=rtpmap:104 speex/32000\r\na=rtpmap:103 speex/16000\r\na=rtpmap:102 speex/8000\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:101 telephone-event/8000\r\na=fmtp:101 0-15\r\na=crypto:1 AES_CM_128_HMAC_SHA1_80 inline:m6DdPZ3gMaUGt/nt7FsgN1PMpDmpICsszxAfFDe5\r\na=crypto:2 AES_CM_128_HMAC_SHA1_32 inline:bObZQmvYtwSQBq1enTNktriqy7prM9y0pFullLWm\r\na=sendrecv\r\n] #[N/A][N/A][a2f1b36ac283434690772af9af2e10f0][N/A][sip:82617034@172.21.99.213(172.21.0.122:5060)][sip:9996100041@172.21.0.122(172.21.0.123:5060)]
*/
/* TEL:
May 18 09:37:04.559 DEBUG  PRISM ////1/[] [udp/172.21.0.123/5060-t-4] #SIP#: (i)[INVITE sip:9996100041@172.21.0.123;user=phone SIP/2.0\r\nContent-Type: application/sdp\r\nTo: sip:9996100041@172.21.0.123\r\nVia: SIP/2.0/UDP 172.21.0.122:5060;branch=z9hG4bK1ig350ghj0szw;rport\r\nVia: SIP/2.0/UDP 172.21.0.123:5060;branch=z9hG4bK1mrx3m5uj1hzn;rport=5060\r\nMin-SE: 1800\r\nRecord-Route: <sip:172.21.0.122:5060;transport=udp;lr>\r\nAllow: UPDATE, BYE, CANCLE, ACK, INVITE, OPTIONS, CANCEL\r\nx-accountid: 5\r\nCall-ID: us3ee1b7nx05\r\nFrom: <tel:+861082825011>;tag=7r0zhikz62m2\r\nMax-Forwards: 69\r\nContact: <sip:172.21.0.123:5060;transport=udp>\r\nx-appid: 4\r\nSession-Expires: 1800\r\nx-sid: 550a9cf2644207c99d5d9b41f325b88d\r\nCSeq: 1 INVITE\r\nContent-Length: 329\r\nSupported: timer, 100rel\r\nx-vdirect: true\r\n\r\nv=0\r\no=- 547512 547512 IN IP4 172.21.0.123\r\ns=voxeo.11.7.72348.0\r\nc=IN IP4 172.21.0.123\r\nt=0 0\r\nm=audio 20136 RTP/AVP 0 8 101 116 9 3 104\r\na=rtpmap:0 PCMU/8000\r\na=rtpmap:8 PCMA/8000\r\na=rtpmap:101 telephone-event/8000\r\na=fmtp:101 0-15\r\na=rtpmap:116 SPEEX/16000\r\na=rtpmap:9 G722/8000\r\na=rtpmap:3 GSM/8000\r\na=rtpmap:104 iSAC/16000\r\n] #[N/A][N/A][us3ee1b7nx05][N/A][tel:+861082825011(172.21.0.122:5060)][sip:9996100041@172.21.0.123(172.21.0.123:5060)]
*/
