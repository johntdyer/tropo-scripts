transfer "tel:+861082825011" {
  :onCallFailure => lambda { |event|
    say "Transfer failure, please check given number or try again later"
  }
}
