result = ask("Hello, this is transfer application. To javen press 1, to hellen press 2.", {
    mode: "dtmf",
    choices: "1,2"
})
if (result.value == 1) 
    transfer("sip:chxd99@172.21.99.111:45678")
else if (result.value == 2)
    transfer("sip:hellen@172.21.99.134:56789")
else
    say("You pressed incorrect digit, please try 1 or 2.")

