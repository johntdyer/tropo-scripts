record "please choose 1, 2, 3, # or *", {
  :timeout => 20.0,
  :choiceMode => "speech",
  :choices => "1,2,3,#,*,one,two,three",
  :onChoice => lambda { |event|
   log "you input " + event.value
  },
  :onTimeout => lambda { |event|
    log "On Timeout!"
  },
  :onEvent => lambda { |event|
    log "On event!"
  }
}
