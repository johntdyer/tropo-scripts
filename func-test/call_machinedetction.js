call(phone, {
		   timeout:30.0,
		      machineDetection: {introduction: "Verifying human or a machine...please hold while we determine...almost finished. Thank you!"},
		         onAnswer: function(event) {
			        say("Detected " + event.value.userType);
				   },  
				      onTimeout: function(event) {
				             say("Call timed out");
					        },  
						   onCallFailure: function(event){ 
						          say("Call could not be complete as dialed");
							     }
							     });

