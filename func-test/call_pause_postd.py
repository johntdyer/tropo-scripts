# This test is to check that after the call is connected
# wait for the time defined by 'pause',
# then dial the DTMF digits defined by 'postd'.
#
# Define the number to dial in the URL:
# https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&phone=sip:vinaduan@sip2sip.info
#
# Testing process:
# step1: launch the application using URL in browser;
# step2: answer the call.

call(phone + ';postd=123;pause=10s',{
		    "timeout":20
		    })
