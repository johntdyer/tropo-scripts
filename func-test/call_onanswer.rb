call $phone, {
   :onAnswer => lambda { |event|
        say "Thanks for calling!"
   },
   :onCallFailure => lambda { |event|
        say "Sorry,please check out your call number!"
   }
}
