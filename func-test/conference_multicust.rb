=begin
  join in conference via REST API - 
  https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&participants=sip:chxd99@ip:port,sip:hellen@ip:port,sip:janey@ip:port,sip:vina@ip:port&conference=1234
  leave conference https://gw.serv.ip.addr/sessions/<session-id>/signals?action=signal&value=chxd99@ip:port
  $curl -L -k -v -H "Content-type: application/json; charset=UTF-8" -X POST -d '{ "value": "hi, there, this is injected message from REST API", "token":"TOKEN"}' https://gw.ser.ip.addr/conferences/conference-ID/say
=end
conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no participant given."
    else
      log "#"*10 + " #{x} join in."
    end

    conferenceOptions = {
      :allowSignals => x,
      :terminator   => "*"
    }
    
    call x, {
      :timeout=>90,
      :onAnswer => lambda { |event|
        newCall = event.value
        newCall.say "You are being invited by the Boss." 
        newCall.conference conferenceID, conferenceOptions
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }
