#join in conference via REST API 
#https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&phone=chxd99
number = $phone
conferenceOptions = {
  :terminator   => "*",
  :joinPrompt   => {"value" => "Hello there, this is tropo, welcome to our conference."},
  :leavePrompt  => {"value" => "You are leaving the conference, goodbye."}
}
call number, {
  :timeout => 90,
  :onAnswer => lambda { |event|
    startCallRecording "ftp://qa_tropo:qa_tropo@ftp.pek.voxeo.com/QA/recordings/conference_signal_recording01.wav"
    conference "1234", conferenceOptions
    #stopCallRecording
  }
}
