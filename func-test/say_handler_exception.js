/*
 * Hangup the phone before it finish the whole sentence, you should the the log message in the sipmethod.log
 * */
answer();
wait(1000);
say("If you work in a Healthcare organization, you are probably grappling with the most dynamic and challenging issues you have ever faced. As a healthcare professional, you must decide how you will meet all these challenges: achieving EHR meaningful use, realizing significant bottom line reductions, and ensuring patients receive the highest quality and safest care possible. You need to act now. Nuance Healthcare can help. Nuance is the only vendor that offers a full range of speech driven clinical documentation and communication solutions and services.");
log("will see this log");
hangup();