ask("What's your four or five digit pin? Press pound when finished", {
    choices:"ÂØπ, Èîô",
    mode:"dtmf",
    onChoice: function(event) {
        say("On Choice!");
    },
    onBadChoice: function(event) {
        say("On Bad Choice!");
    },
    onTimeout: function(event) {
        say("The answer is timeout!");
    },
    onError: function(event) {
        say("if you hear this, it means there is an error");
    }
});
