startCallRecording "file:///recordings/test.wav"
ask "What's your favorite color? Choose from red, blue or green.", {
	    :bargein => false,
		        :choices => "red, blue, green",
			    :attempts => 3}
			    stopCallRecording
			    say "call recording has been stopped."
wait 2000
say " you said file:///recordings/test.wav"
