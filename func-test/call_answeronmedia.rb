#launch the call via REST API 
#https://gw.serv.ip.addr/sessions?action=create&token=TOKEN
call $phone, {
  :timeout => 10.0,
  :answerOnMedia => true,
  :onAnswer => lambda {
    log "Thanks for calling!"
  },
  :onTimeout => lambda {
    log "On timeout!"
  }
}
