conferenceID = "1234" 
phonelist = [] 
phonelist = $phones.split(",") 
 
threads = [] 
phonelist.each do |x| 
  threads << Thread.new do
    x = x.to_s
    if x.nil?
      log "#"*10 + " no phone number given."
    elsif x == "mjfsdrcg"
      ip = "172.21.99.132"
      port = "59798"
    elsif x == "lcdgoxzb"
      ip = "172.21.99.132"
      port = "59800"
    else
      log "#"*10 + " incorrect phone number."
    end
    
    call 'sip:' + x + '@' + ip + ':' + port, {
      :allowSignals => x,
      :answerOnMedia => true,
      :timeout=>90,
      :onAnswer => lambda do |event|
        log "@"*10 + "Invitee answered join conference " + conferenceID
        log "@"*10 + "The signal is "+ x
        newCall = event.value
        log "@"*10 + "The new call id is "+ newCall.sessionId
        #newCall.say "You are being invited by the Boss." 
        log "@"*10 + "The conferenceID is " + conferenceID
	#$appInstance.block(10000)
        newCall.conference conferenceID, {
          :allowSignals => x,
	  :terminator   => "*",
          :joinPrompt   => "Hello, welcome to the conference.",
          :leavePrompt  => "You are leaving the conference, goodbye."
        }
        log "************* " + x + " is kicked out."
	#newCall.hangup
      end
    }
  end
end
 
=begin
threads.each do |t| 
  t.join  
end
=end

#$appInstance.block(10000)
wait 30000
