<?php
/* Transfer - on connect, 'transferTo' can hear say("Transfer new destination success")
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   "transferFrom" will get ring first, then 'transferFrom' answer the call, after that 'transferTo' will get the ring -> 'transferTo' answer the call & hear the say of transfer success
*/

//$result = call("$transferFrom");
call("$transferFrom");
transfer("$transferTo", array(
    "terminator"=> "#",
    "onConnect" => "connectFNC",
    "onSuccess" => "successFNC"
    )
);
function connectFNC($event) {
    //record 1st leg
    //$result.startCallRecording("ftp://public:public@10.75.187.217/QA/recordings/transfer.mp3", array("format" => "audio/mp3"));
    startCallRecording("ftp://public:public@10.75.187.217/QA/recordings/transfer.mp3", array("format" => "audio/mp3"));
    say("Transfer new destiniation success.");
    //$result.stopCallRecording();
    stopCallRecording();
};
function successFNC($event) {
    say("Transfer end success, you will hear the recording file.");
    say("ftp://public:public@10.75.187.217/QA/recordings/transfer.mp3");
};
?>
