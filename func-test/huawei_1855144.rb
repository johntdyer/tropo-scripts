#reproduce huawei ticket 1855144
#conference handler
call "sip:phono@sip2sip.info", {
  :timeout => 10,
  :onAnswer => lambda { |event|
    newCall = event.value
    wait 30000
    newCall.say("say first")
    log "log 1st"
    wait 30000
    newCall.say("say second")
    log "log 2nd"
    wait 30000
    newCall.conference("1234", { :terminator => "*", :playTones => true})
    log "log 3rd"
  },
  :onTimeout => lambda { |event|
    log "time out"
  },
  :onCallFailure=> lambda { |event|
    log "call failure"
  }
}

