session_id = $currentCall.sessionId
ftp_url = "ftp://public:public@10.75.187.217/QA/recordings/" + session_id + ".mp3"
record "how you feel!", {  
  :bargein => false,
  :recordURI => ftp_url,
  :timeout => 20.0,
  :onTimeout => lambda { |event|
    log "On Timeout!"         
  },
  :onEvent => lambda { |event|
    log "On event!"
  }
}
wait 1000
say "Will play the recorded file."
wait 2000
say ftp_url
