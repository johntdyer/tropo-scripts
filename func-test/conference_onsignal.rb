#join in conference via REST API - https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&participants=sip:chxd99@ip:port,sip:hellen@ip:port,sip:janey@ip:port,sip:vina&conference=1234
#exit conference https://172.21.0.122/sessions/<session-id>/signals?action=signal&value=exit
conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no phone number given."
    else
      log "#"*10 + " #{x} join in."
    end

    conferenceOptions = {
      :allowSignals => x,
      :terminator   => "*",
      :onSignal => lambda { |event| log("========> Good-bye!")}
    }
    
    call x , {
      :timeout=>130,
      :onAnswer => lambda { |event|
        newCall = event.value
        newCall.say "You are being invited by the Boss." 
        newCall.conference conferenceID, conferenceOptions
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }
