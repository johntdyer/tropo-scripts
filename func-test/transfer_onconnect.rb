#Whisper on Transfer Example

transfer "sip:alice@10.140.254.24:5061", {
		    :playvalue => "hello, we are transfer you to the callee now, please wait a few minutes",
			    		    :terminator => "*",
					    		    :allowSignals => "#",
		:onConnect => lambda { |even|													        ask "Press 1 to accept the call, press any other key to reject.", {
				:choices => "1"	,
				:mode => "dtmf",
				:onChoice => lambda { |event|	say "Connecting you now"},
				:onBadChoice => lambda { |event|										say "Rejecting the call. Goodbye."
				       	hangup
			      	} 
	       			}
	       	              },
			        :onSignal => lambda { |event|
				say "you get a signal, your call will ended"
			   },
				:onRinging => lambda { |event|
					setTag "tropo-jack-ring"
			    # event.value.setTag("tropo-jack"); //leg B	
						log "It's ringing for transferee!!! "
																			      },

}

