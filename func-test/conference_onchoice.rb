conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no phone number given."
    else
      log "#"*10 + " #{x} join in."
    end
=begin
    conferenceOptions = {
      :terminator => "*",
      :onChoice => lambda { |event|
        newCall.say "Disconnecting"
        },
      :onHangup => lambda { |event|
        log " On Hangup!"
        }
    }
=end    
    call x , {
      :timeout=>90,
      :onAnswer => lambda { |event|
        newCall = event.value 
        newCall.conference conferenceID, {
         :terminator => "*",
         :onChoice => lambda { |event|
             newCall.say "Disconnecting"
           },
         :onHangup => lambda { |event|
             log " On Hangup!"
           }
        }
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }