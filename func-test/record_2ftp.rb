def ask_format
  result = ask "Which audio format do you want to record, press 1 for w a v, 2 for mp3, 3 for a u?", {
    :mode => "dtmf",
    :choices => "1, 2, 3"
  }
  return result.value
end

def record_now(ftp_url, audio_format)
  record "Tell us what your favorite color is. Press pund key to terminate recording.", {
    :recordMethod => "POST",
    :recordFormat => "#{audio_format}",
    :terminator => "#",
    :recordURI => "#{ftp_url}"
  }
end

for i in 1..3
  case ask_format
  when '1'
    chose_format = "audio/wav"
    say "you choose w a v format"
  when '2'
    chose_format = "audio/mp3"
    say "you choose mp3 format"
  when '3'
    chose_format = "audio/au"
    say "you choose a u format"
  else
    chose_format = "audio/wav"
    say "wrong choice, will use the default record format"
  end
  session_id = $currentCall.sessionId
  ftp_url = "ftp://public:public@10.75.187.217/QA/recordings/" + session_id + chose_format.gsub("audio/", ".")
  record_now ftp_url, chose_format
  say "You will hear your recorded file," + ftp_url
end

say "End recording, goodbye!"
