#conference_prompt.rb
conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no phone number given."
    else
      log "#"*10 + " #{x} join in."
    end

    conferenceOptions = {
      :terminator   => "*",
      :joinPrompt   => {"value" => "someone joined"},
      :leavePrompt  => {"value" => "http://www.phono.com/audio/troporocks.mp3"}
    }
    
    call x , {
      :timeout=>90,
      :onAnswer => lambda { |event|
        newCall = event.value 
        newCall.conference conferenceID, conferenceOptions
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }