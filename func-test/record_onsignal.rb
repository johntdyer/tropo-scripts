call $phone, {
	:allowSignals => "#",
	:onAnswer => lambda { |event| 
		session_id = $currentCall.sessionId
		record "hello, this is a testing signal script", {
			:allowSignals => "exit",
			:recordURI => "ftp://public:public@10.75.187.217/QA/recordings/" + session_id + ".wav",
			:onSignal  => lambda { |event|
				say "got the signal!"
			}
		}
	}
}
