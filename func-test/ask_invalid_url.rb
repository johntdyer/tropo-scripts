ask "",{
  :choices => "[4-5 DIGITS]",
  :terminator => '#',
  :timeout => 15.0,
  :mode => "dtmf",
  :interdigitTimeout => 5 ,
  :onChoice => lambda { |event|
    say "On Choice"
  },
  :onBadChoice => lambda { |event|
    say "On Bad Choice"
  }
}
