#passing variable(s) to call via REST API
#https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&number=chxd99
call 'sip:' + $number + '@172.21.99.111:45678', {
  :onAnswer => lambda { |event|
    say "Thanks for calling!"
  },
  :onCallFailure => lambda { |event|
    say "Sorry,please check out your call number!"
  }
}
