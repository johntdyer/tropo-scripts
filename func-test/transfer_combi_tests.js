answer();
startCallRecording("ftp://10.75.187.217/QA/recordings/record_javen.mp3",
{
    format: "audio/mp3",
    recordUser: "public",
    recordPassword: "public",
});
/*list sip clients and accounts, e.g.
 *jitsi-sip:evelyn@172.21.91.119:5060
 *xlite-sip:evelyn@172.21.91.119:6060
 *xliet-sip:chxd99@172.21.99.176:45678
 *notice that jitsi cannot hear postd.
 */
transfer(["45693820@10.140.254.131:5066;postd=12345", "linphone@10.140.254.24:5061"], 
{
    tag: "voxeolabs-jack", //leg B
    playvalue: "hello, will start to play local audio now http://127.0.0.1:8080/tropo/script/troporocks.mp3",
    terminator: "*",
    playTones: false ,
    answerOnMedia: false,
    interruptPlayValue: false,
    onRinging: function(event) {
        //setTag("tropo-jack-ring"); //leg A
        event.value.setTag("tropo-jack"); //leg B
        log("It's ringing for transferee!!! [" + event.value.calledID + "]");
    },
    onConnect: function() {
        setTag("cisco-jack"); //leg B
        //event.value.setTag("cisco-jack"); //also leg B
        log("transfer successully.");
        say("you are the destination number.");
    },
    onError: function() {
        //Illegal phone number or SipUri for callee, like sip:锟铰~\漫F锟铰~\漫F锟铰~[锟嚼10.140.254.131
        setTag("cisco-jack-error"); //leg A
        log("@@@@bad result - Fatal Error");
    },
    onCallFailure: function() {
        //callee offline or reject
        setTag("cisco-jack-fail"); //leg A
        log("@@@@bad result - onCallFailure");
    },
    onTimeout: function() {
        setTag("cisco-jack-timeout"); //leg A
        //when recieve terminator or timeout
        log("@@@@bad result - onTimeout");
    },
    onBusy: function() {
        setTag("cisco-jack-busy"); //leg A
        log("Failed to transfer the call to a busy number!!");
    },
    onHangup: function() {
        log("@@@@bad result - onHangup");
    },
    onSignal: function() {
        log("@@@@bad result - transfer interrupted.");
    },
    onSuccess: function() {
        setTag("cisco-javen"); //leg A
        log("@@@@good result");
        say("transfer end, BYE!");
    }
});
stopCallRecording();
log("end transfer.");
say("BYE");
