/*
 *lauch call via REST API - https://gw.serv.ip.addr/sessions?action=create&token=TOKEN
 */
call(["sip:chxd99@172.21.99.111:45678", "sip:hellen@172.21.99.121:56789", "+861082825011", "tel:+861082825012"], {
   onAnswer: function() {
       say("Thanks for calling!");
   }
});
