sessionid = currentCall.sessionId;
log("The Session ID is " + sessionid);
ask("What's your four or five digit pin? Press pound when finished", { 
    choices:"[4-5 DIGITS]", 
    terminator:"#", 
    timeout:30, 
    mode:"dtmf",
    allowSignals: "exit",
    interdigitTimeout: 5, 
    voice: "malcolm",
    onChoice: function(event) { 
        say("On Choice!"); 
    }, 
    onBadChoice: function(event) { 
        say("On Bad Choice!"); 
    }, 
    onTimeout: function(event){
       say("The answer is timeout!");
    },
    onEvent:function(event){
       say("This ask is finished, welcome to ask us next time!");
    },
    onError: function(event) {
       say("if you hear this, it means there is an error happened.")
    }
});
