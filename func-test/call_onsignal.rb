#the script will be interrupted when received http signal request
#lauch the call via REST API
#https://gw.serv.ip.addr/sessions?action=create&token=TOKEN
#https://gw.serv.ip.addr/sessions/<session-id>/signals?action=signal&value=*
#uncomment the following two lines if launched call via sip phone cline
#session_id = $currentCall.sessionId
#log "session-id is " + session_id
call $phone, {
  :allowSignals => "*",
  :onAnswer => lambda { |event|
    say "While having lots of options available in a distribution is great for Linux geeks," +
        "it can become a nightmare for beginning Linux users. " +
        "While having lots of options available in a distribution is great for Linux geeks," +
        "it can become a nightmare for beginning Linux users. "
  },
  :onSignal => lambda { |event|
    say "Received signals, will be interrupted."
  }
}

