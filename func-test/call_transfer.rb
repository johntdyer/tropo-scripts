#Verify ticket TROPO-1650
#https://gw.serv.ip.addr/sessions?action=create&token=TOKEN
#https://gw.serv.ip.addr/sessions/<session-id>/signals?action=signal&value=*
#session_id = $currentCall.sessionId
#log "session-id is " + session_id
caller = "sip:hellen@172.21.99.231:56789" 
callee = "sip:chxd99@172.21.99.111:45678" 
call caller, { 
  :allowSignals => "exit_call", 
  :onAnswer => lambda { |event| 
    transfer callee, { 
      :allowSignals => "exit_transfer", 
      :terminator => "*",
      :onSignal => lambda { |event|
       # say "receive the signal!"
        log "receive the signal!"
      }
    } 
      say "it's the second time to hear!"
  } 
} 
say "it's the third time to hear!"
