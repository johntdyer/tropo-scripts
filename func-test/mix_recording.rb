def ask_format
  result = ask "Which audio format do you want to record, press 1 for w a v, 2 for mp3, 3 for a u?", {
    :mode => "dtmf",
    :choices => "1, 2, 3"
  }
  return result.value
end

def record_now(ftp_url, audio_format=".wav")
  record "Tell us what's your favorite color? Press pund key to terminate recording.", {
    :recordMethod => "POST",
    :terminator => "#",
    :recordURI => "#{ftp_url}test1#{audio_format}" 
  }
end

for i in 1..3
  case ask_format
  when '1'
    chose_format = ".wav"
  when '2'
    chose_format = ".mp3"
  when '3'
    chose_format = ".au"
  else
    chose_format = ".wav"
  end
  ftp_url = "ftp://public:public@10.75.187.217/QA/recordings/"
  startCallRecording ftp_url + "test2" + chose_format
  record_now ftp_url, chose_format 
  stopCallRecording
  say "Your will hear your recorded file by record API," + ftp_url + "test1" + chose_format
  say "Your will hear your recorded file by StartCallRecording API," + ftp_url + "test2" + chose_format
end

say "End recording, goodbye!"
