# need to check the vcs log, if the Nuance recogniser server is used during the test.

ask "What's your favorite color? Choose from red, blue or green.", {
    :choices => "red, blue, green",
    :recognizer => "en-gb",
    :mode => "speech",
    :onChoice => lambda { |event|
        say "On Choice"
    },
    :onBadChoice => lambda { |event|
        say "On Bad Choice"
    }
}

