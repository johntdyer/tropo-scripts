call $phone, {
	   :timeout => 30,
		      :machineDetection => {"introduction" => "Verifying human or a machine...please hold while we determine...almost finished. Thank you!"},
		         :onAnswer => lambda {  |event|     
				      say "Detected " + event.value.userType},
			    :onTimeout => lambda { |event|
				         say "Call timed out"},
			       :onCallFailure => lambda { |event|
				            log "Call could not be complete as dialed"}
}

