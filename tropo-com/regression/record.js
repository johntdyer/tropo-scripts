/** when the enable.early.media set to false, then the record prompt can played correctly no matter answer or not and the record file is good. 
when the enable.early.media set to true: 
If answer before record, the record prompt can played correctly no matter answer or not and the record file is good. if answer in onError, then the error prompt will played, and the record will fail. 
If no answer in the call, the call will failed directly.
**/


answer()
record("Tell us how you feel in fifteen minutes or less!", {
		    beep:true,
		        maxTime:900,
			    recordURI:"ftp://public:public@10.75.187.217/QA/recordings/7674record.mp3",
			      onError: function(event) {
			             if(event.name == "earlyMediaFailure") {
				               answer();
					                 say("early media failure");
							        }
								   }
								       }
      ); 
