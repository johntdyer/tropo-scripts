require 'net/https'
require "uri"
require 'json'
require 'csv'


@basic_url= "https://api.tropo.com/v1/applications"
@username = "jackwliu"
@password = ARGV[0]

# ruby generate_csv4tts.rb <password_4_jackwliu> 5050651 5050735
@app_id_m = ARGV[1].to_i # 5050651
@app_id_n = ARGV[2].to_i # 5050735
@loop_time= @app_id_n - @app_id_m

for i in 0..@loop_time
	@app_id = @app_id_m + i
	
	# https://api.tropo.com/v1/applications/5050735/addresses
	# https://api.tropo.com/v1/applications/5050735
	uri_1 = URI.parse(@basic_url + "/#{@app_id}" + "/addresses")
	uri_2 = URI.parse(@basic_url + "/#{@app_id}" )
	
	http_1 = Net::HTTP.new(uri_1.host, uri_1.port)
	http_2 = Net::HTTP.new(uri_2.host, uri_2.port)
	http_1.use_ssl = true
	http_2.use_ssl = true

	request_1 = Net::HTTP::Get.new(uri_1.request_uri)
	request_2 = Net::HTTP::Get.new(uri_2.request_uri)
	request_1.basic_auth @username, @password unless @username.nil?
	request_2.basic_auth @username, @password unless @username.nil?

	response_1 = http_1.request(request_1)
	response_2 = http_2.request(request_2)

	# response.body
	# response.status

	parsed_json_1 = JSON.parse(response_1.body)
	parsed_json_2 = JSON.parse(response_2.body)
	# puts JSON.pretty_generate(parsed_json_1)
	# puts JSON.pretty_generate(parsed_json_2)
	# puts parsed_json_1[0]['applicationId']
	# puts parsed_json_2['id']
	applicationId = parsed_json_1[0]['applicationId']
	id = parsed_json_2['id']
	# puts applicationId
	# puts id


	if applicationId.to_s == id.to_s
		@pin_no = parsed_json_1[0]['address']
		@v_name = parsed_json_2['name']

		file_name = 'tts.csv'
		if File.exist?(file_name)
			CSV.open(file_name, "a+") do |csv|
		  		csv << ["180878", @app_id, @pin_no, @v_name]
			end
		else
			CSV.open(file_name, "wb") do |csv|
		  		csv << ["user_id", "app_id", "sip_address", "app_name"]
		  		csv << ["180878", @app_id, @pin_no, @v_name]
			end
		end
	else
		puts "Wrong Applications!!"
	end

end

