flag = false;
ask("hello there", {
    bargein:false,
    recognizer: "en-za",
    choices:"hello world",
    timeout:60,
    onChoice: function(event) {
    	flag = true;
    },
    onBadChoice: function(event) {
    	flag = false;
    }
});
wait(12000);
if (flag) {
	say("Good, The ASR is recognized successfully.");
} else {
	say("Sorry, failed to recognize The ASR.");
};
