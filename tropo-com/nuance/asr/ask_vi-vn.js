flag = false;
ask("hello there", {
    bargein:false,
    recognizer: "vi-vn",
    choices:"Chào",
    timeout:60,
    onChoice: function(event) {
    	flag = true;
	say("good choice");
    },
    onBadChoice: function(event) {
    	flag = false;
	say("bad choice");
    }
});
wait(12000);
if (flag) {
	say("Good, The ASR is recognized successfully.");
} else {
	say("Sorry, failed to recognize The ASR.");
};
