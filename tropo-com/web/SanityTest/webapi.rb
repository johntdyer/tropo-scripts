require 'rubygems'
require 'sinatra'
require 'tropo-webapi-ruby'
require 'socket'
require 'json'

#get my ip
my_ip = ""
addr_infos = Socket.ip_address_list
addr_infos.each do |x|
  my_ip = x.ip_address if x.ip_address =~ /10./
end

puts "###################################################################"
puts "app url for commnader is http:\/\/#{my_ip}:4567\/index.json"
puts "###################################################################"

set :bind, my_ip
rec_url = 'ftp://public:public@10.75.187.217/QA/recordings/webapi_recording.mp3'
mp3_file = " https://ccrma.stanford.edu/~jos/mp3/gtr-jazz.mp3"
wav_file = 'ftp://public:public@10.75.187.217/QA/recordings/webapi_start_recording.wav'
x_lite_sip = "sip:x-lite@#{my_ip}:5062"

post '/index.json' do
  t = Tropo::Generator.new
  t.start_recording :url => wav_file
  t.ask :name => 'test_scenario', 
        :timeout => 60, 
        :bargein => false,
        :say => {:value => "What test do you want to do?  Choose from transfer, conference or recording."},
        :choices => {:value => "transfer, conference, recording"}

 t.stop_recording
 t.say :value => "Here is the start call recording " + wav_file
 t.on :event => 'continue', :next => '/continue.json'

  s = t.response
  p s
end

post '/continue.json' do
  v = Tropo::Generator.parse request.env["rack.input"].read
  t = Tropo::Generator.new
  answer = v[:result][:actions][:test_scenario][:value]
  t.say(:value => "You said " + answer + mp3_file)
  case answer
  when "transfer"
    t.on :event => 'continue', :next => '/transfer.json'
  when "conference"
    t.on :event => 'continue', :next => '/conference.json'
  when "recording"
    t.on :event => 'continue', :next => '/recording.json'
  else
    t.say("Sorry, no match")
  end
  t.response
 
end

post '/transfer.json' do
  t = Tropo::Generator.new
  t.say(:value => "We are transfering you, please wait")
  t.transfer(:to => x_lite_sip)
  t.response
end

post '/conference.json' do
  t = Tropo::Generator.new
  t.say("Welcome to the conference!")
  t.conference(:name => "conference", :id => 1234)
  t.response
end

post '/recording.json' do
  t = Tropo::Generator.new do 
    on :event => 'continue', :next => '/playback.json'
    record({
    :name => "record",
    :format => "audio/wav",
    :url => rec_url,
    :maxTime => 10
  }) do 
       say :value => "please leave a message!" 
     end
   end
  t.response 
end

post '/playback.json' do
  t = Tropo::Generator.new 
  t.say(:value => "Here is the recording file")
  t.say(:value => rec_url)
end
