ask "", {
    :choices => "https://bitbucket.org/voxeolabs/tropo-scripts/raw/d91e0b8a1e49fc1d6b468d3cbcd7a0214a4bc8ac/tropo-com/gram/ask_grammar.xml",
    :terminator => '#',
    :timeout => 15.0,
    :interdigitTimeout => 5 ,
    :onChoice => lambda { |event|
        log "On Choice " + event.choice.interpretation
    },
    :onBadChoice => lambda { |event|
        log "On Bad Choice"
    }
}

=begin
 # //send_msg.js
 # if (typeof(phone) != 'undefined' && typeof(callerid) != 'undefined') {
 #      message(msg, {
 #          to: phone,
 #          network: "SMS",
 #          callerID: callerid
 #      });
 #  } else if (typeof(phone) != 'undefined' && typeof(callerid) == 'undefined') {
 #      message(msg, {
 #          to: phone,
 #          network: "SMS"
 #      });
 #  } else
 #      log('####Received respond sms ' + currentCall.initialText);
=end