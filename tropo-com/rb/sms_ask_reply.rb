ask "please choice four or five digits, Press pound when finished", {
    :choices => "[4-5 DIGITS]",
    :terminator => '#',
    :timeout => 5.0,
    :mode => "dtmf",
    #:interdigitTimeout => 5 ,
    :onChoice => lambda { |event|
        say "On Choice"
    },
    :onBadChoice => lambda { |event|
        say "On Bad Choice"
    },
    :onTimeout => lambda { |event|
        say "On Timeout"
    }
}


 #	//send_msg.js
 #	if (typeof(phone) != 'undefined' && typeof(callerid) != 'undefined') {
 #		message(msg, {
 #			to: phone,
 #			network: "SMS",
 #			callerID: callerid
 #		});
 #	} else if (typeof(phone) != 'undefined' && typeof(callerid) == 'undefined') {
 #		message(msg, {
 #			to: phone,
 #			network: "SMS"
 #		});
 #	} else
 #		log('####Received respond sms ' + currentCall.initialText);
