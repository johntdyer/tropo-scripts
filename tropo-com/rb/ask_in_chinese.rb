result = ask "你最喜欢的颜色?  红，黄，还是蓝", {
  :choices => "红, 黄, 蓝",
  :recognizer => "zh-cn",
  :voice => "Tian-tian"
}
say "你选的是 #{result.value}",{:voice => 'Tian-tian'}
log "They said " + result.value