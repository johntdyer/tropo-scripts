EMPHASIS = <<-STR 
  <?xml version="1.0"?>
  <speak version="1.0" xmlns="http://www.w3.org/2001/10/synthesis"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://www.w3.org/2001/10/synthesis
                     http://www.w3.org/TR/speech-synthesis/synthesis.xsd"
           xml:lang="en-US">
 <voice name="malcolm">
    That is a <emphasis> big </emphasis> car!
    That is a <emphasis level="strong"> huge </emphasis>
    bank account!
 </voice> </speak>
STR

say EMPHASIS