##https://gateway_ip/sessions?action=create&token=TOKEN&scenario=SCENARIO&test_case=TEST_CASE&address=ADDRESS&secondAddress=SECONDADDRESS


## Inbound Test
## This test will simply play a wave file and put the user into a conference for 120 seconds by default
# call helloworld@{gatewayIP}

## Outbound Test ( simple )
# action=create
# scenario=call
# test_case=mp3
# address=sip:someone@domain.com

## Outbound Test ( Conference )
# action=create
# scenario=call
# test_case=conference
# address=sip:jdyer@sip2sip.info

## Outbound Test ( ASR )
# action=create
# scenario=call
# test_case=ask
# address=sip:jdyer@sip2sip.info


## Outbound Test ( TTS )
# action=create
# scenario=call
# test_case=tts
# address=sip:jdyer@sip2sip.info
# tts ( optional ) - TTS to be spoken
# voice ( optional ) - TTS voice to use

## Transfer Test
# action=create
# scenario=transfer
# address=sip:someone@domain.com
# secondAddress=sip:someoneElse@otherdomain.com

## Outbound Test ( Record )
# action=create
# scenario=call
# test_case=record
# address=sip:jdyer@sip2sip.info

## Outbound Test ( Startcallrecording )
# action=create
# scenario=call
# test_case=startcallrecording
# address=sip:jdyer@sip2sip.info

log "######## [TEST] Started helloworld test application"

####### START TEST FUNCTIONS ########

def start_kill_switch(time=nil)
    timer = time || 120
    Thread.new do
        log "######## [TEST] Kill switch in 120 seconds"
        sleep timer
        log "######## [TEST] Kill switch timer elapsed"
        say "Kill switch timer elapsed"
        hangup
    end
end

def test_tts(data,voice=nil)
    data = data || "this is a test"
    if voice
        say data, {:voice => voice}
    else
        say data
    end
    #$options = voice ? {:voice => voice} : Hash.new
    #say data, options
end

def test_asr
    log "######## [TEST] Test ASR"
    say "Lets test speech recognition"
    result = ask "What's your favorite color?  Choose from red, blue or green.", {
        :mode => "speech",
        :choices => "red, blue, green",
        :attempts => 3
    }
    say "You chose " + result.value
    log "######## [TEST] ASR=#{result.value}"
end

def test_dtmf
    log "######## [TEST] Test DTMF"
    result = ask "lets test DTMF recogition.  Please pick a number from 0 to 9", {
        :mode => "dtmf",
        :choices => "0,1,2,3,4,5,6,7,8,9"
    }
    say "You said " + result.value
    log "######## [TEST] DTMF=#{result.value}"
end

def play_tropo_rox
    say "http://127.0.0.1:8080/tropo/script/troporocks.mp3"
end

def test_transfer(address,secondAddress=nil)
    if secondAddress
        log "######## [TEST] Transfer:  {:first=>#{address}, :second => #{secondAddress}}"
        call address
        say "Now transfering you to second address"
        transfer secondAddress,{:callerID => address, :terminator => "#"}
    else
        call address
        say "Now transfering you to zipdx test application"
        transfer "sip:3366@login.zipdx.com"
    end
end

def test_record
	log "######## [TEST] Test Record"
	record "Tell us how you feel!", {
				 :recordURI => "ftp://public:public@10.75.187.217/QA/recordings/test_record.mp3",
				:recordFormat =>"audio/mp3",
				:beep => true,
				:maxTime => 10,
				:terminator => "#"
 }
 	wait(5000)
 	say "You will hear your recorded file, ftp://public:public@10.75.187.217/QA/recordings/test_record.mp3 "
end

def test_startCallRecording
	log "######## [TEST] Test StartCallRecording"
	startCallRecording "ftp://public:public@10.75.187.217/QA/recordings/test_startCallRecording.mp3",{
			:format =>"audio/mp3"
			}
ask "What's your favorite color?  Choose from red, blue or green.", {
   :choices => "red, blue, green",
   :attempts => 3}
	stopCallRecording
	wait(5000)
 	say "You will hear your recorded file, ftp://public:public@10.75.187.217/QA/recordings/test_startCallRecording.mp3 "
end
####### END TEST FUNCTIONS ########
if $action.eql?('create')
    case $scenario
        when 'call' then call $address
        when 'transfer' then test_transfer($address,$secondAddress)
    end

    if $test_case.eql?('ask')
        test_asr
        test_dtmf
    elsif $test_case.eql?('tts')
        test_tts($tts,$voice)
    elsif $test_case.eql?('conference')
        conference('hello_world')
    elsif $test_case.eql?('mp3')
        play_tropo_rox
    elsif $test_case.eql?('record')
        test_record
    elsif $test_case.eql?('startcallrecording')
        test_startCallRecording
    end

else
    say "Welcome to the test application"
    play_tropo_rox

    start_kill_switch(120)

    conference('hello_world')

end

say "goodbye"
log "######## [TEST]  Finished helloworld test application"
hangup
