ask "", {
    :choices => "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/recev_unicode_grammar.xml",
    :terminator => '#',
    :timeout => 15.0,
    :interdigitTimeout => 5 ,
    :onChoice => lambda { |event|
        log "On Choice " + event.choice.interpretation
    },
    :onBadChoice => lambda { |event|
        log "On Bad Choice"
    }
}


 #  //send_msg.js
 #  if (typeof(phone) != 'undefined' && typeof(callerid) != 'undefined') {
 #      message(msg, {
 #          to: phone,
 #          network: "SMS",
 #          callerID: callerid
 #      });
 #  } else if (typeof(phone) != 'undefined' && typeof(callerid) == 'undefined') {
 #      message(msg, {
 #          to: phone,
 #          network: "SMS"
 #      });
 #  } else
 #      log('####Received respond sms ' + currentCall.initialText);
