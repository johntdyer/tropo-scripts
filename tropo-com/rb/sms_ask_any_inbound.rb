ask "", {
    :choices => "[ANY]",
    :terminator => '#',
    :timeout => 15.0,
    :mode => "dtmf",
    :interdigitTimeout => 5 ,
    :onChoice => lambda { |event|
        log "On Choice"
    },
    :onBadChoice => lambda { |event|
        log "On Bad Choice"
    }
}


 #    //send_msg.js
 #    if (typeof(phone) != 'undefined' && typeof(callerid) != 'undefined') {
 #        message(msg, {
 #            to: phone,
 #            network: "SMS",
 #            callerID: callerid
 #        });
 #    } else if (typeof(phone) != 'undefined' && typeof(callerid) == 'undefined') {
 #        message(msg, {
 #            to: phone,
 #            network: "SMS"
 #        });
 #    } else
 #        log('####Received respond sms ' + currentCall.initialText);
