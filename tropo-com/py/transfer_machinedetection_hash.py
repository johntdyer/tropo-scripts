call(transferFrom)
transfer(transferTo,{
    "playvalue": "https://www.tropo.com/static/audio/tropo-rocks.mp3",
    "playrepeat": 3,
    "timeout":60,
    "allowSignals":"tres",
    "machineDetection": {"introduction": 'hello welcome to tropo, the text is introduction to test machine detection.', "voice": 'Kate'},
    "onConnect": lambda event : say("Machine detection is successful , CPA usertype is " + str(event.value.userType)) and log("==========> CPA usertype=" + str(event.value.userType)),
    "onSignal": lambda event : say("Signal input, transfer will end") and log("==========> Signal input, transfer will end")
})
