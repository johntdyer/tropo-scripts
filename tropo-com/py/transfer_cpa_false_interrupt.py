##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
call(transferFrom)
transfer(transferTo, {
    "timeout":60,
    "allowSignals":"#",
    "terminator":"*",
    "machineDetection": {
        "introduction": 'https://www.tropo.com/static/audio/tropo-rocks.mp3',
        "voice": 'kate',
        "interruptIntro": False},
    "onConnect": lambda event : say("dtmf input is " + str(event.value.input) + ", and CPA usertype is " + str(event.value.userType)),
    "onSignal": lambda event : log("==========> the call is interrupted")
})
