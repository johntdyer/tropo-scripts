##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
call(toDial, {
    "timeout":60,
    "allowSignals":"#",
    "machineDetection": {
        "introduction": 'https://www.tropo.com/static/audio/tropo-rocks.mp3',
        "voice": 'kate',
        "interruptIntro": False },
    "onAnswer": lambda event : say("dtmf input is " + str(event.value.input) + ", and CPA usertype is " + str(event.value.userType)),
    "onSignal": lambda event : log("==========> the call is interrupted")
})
