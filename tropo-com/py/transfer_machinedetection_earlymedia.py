call(transferFrom)
transfer(transferTo,{
    "timeout":60,
    "voice":"Victor",
    "answerOnMedia":True,
    "machineDetection": {"introduction": 'Hello! When working with multinational callers, American English might not be the language.The nitty gritty gospel behind every Tropo example, broken down and explained piece by piece. Want to know if a parameter takes an Integer or a Float? Need to know every possible option available on record?'}, 
    "onConnect": lambda event : say("CPA usertype is " + str(event.value.userType))
})
