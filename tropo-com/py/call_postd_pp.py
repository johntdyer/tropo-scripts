# This test is to check that after the call is connected
# dial 123 and pause for an additional 5 seconds (ppppp) and then dial 456.
#
# Define the number to dial in the URL:
# https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
#
# Testing process:
# step1: launch the application using URL in browser;
# step2: answer the call.

call(toDial + ';postd=123ppppp456',{
    "timeout":20
})