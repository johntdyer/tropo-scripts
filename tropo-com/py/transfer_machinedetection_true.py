call(transferFrom)
transfer(transferTo,{
    "timeout":60,
    "machineDetection": True,
    "onConnect": lambda event : say("Machine detection is successful , CPA usertype is " + str(event.value.userType)) and log("==========> CPA usertype=" + str(event.value.userType)),
    "onSignal": lambda event : say("Signal input, transfer will end") and log("==========> Signal input, transfer will end")
})
