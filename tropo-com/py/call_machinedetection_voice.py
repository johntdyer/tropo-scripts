##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
call(toDial, {
    "timeout":60,
    "voice":"Susan",
    "machineDetection": {
        "introduction": 'Hello! When working with multinational callers, American English might not be the language. https://www.tropo.com/static/audio/tropo-rocks.mp3',
        "voice": 'Victor'},
    "onAnswer": lambda event : say("Welcome to Tropo!") and log("==========> CPA usertype=" + str(event.value.userType))
})
