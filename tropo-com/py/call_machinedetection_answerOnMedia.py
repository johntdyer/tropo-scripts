##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
call(toDial, {
    "timeout":60,
    "voice":"Victor",
    "answerOnMedia":True,
    "machineDetection": {
        "introduction": 'Hello! When working with multinational callers, American English might not be the language.The nitty gritty gospel behind every Tropo example, broken down and explained piece by piece. Want to know if a parameter takes an Integer or a Float? Need to know every possible option available on record?'},
    "onAnswer": lambda event : log("==========> On answer! The CPA usertype=" + str(event.value.userType))
})
