# This test is to check that when the call is on busy, 
# the event 'onBusy' fires.
#
# Define the number to dial in the URL:
# https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
#
# Testing process:
# step1: make sure the number to dial is in the call;
# step2: launch the application using URL in browser;
# step3: check the content of log() from server logs.

call(toDial,{
    "timeout":20,
    "onBusy": lambda event : log("==========On busy! Wait for a moment!")
})
