# This test is to check that when calling an incorrect 
# or disconnected destination phone number, 
# the event 'onCallFailure' fires.
#
# Define the number to dial in the URL:
# https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
#
# Testing process:
# step1: set an incorrect or disconnected phone number in the URL;
# step2: launch the application using URL in browser;
# step3: check the content of log() from server logs.

call(toDial,{
    "timeout":20,
    "onCallFailure": lambda event : log("===========Call failure!The number is incorrect or disconnected!")
})
