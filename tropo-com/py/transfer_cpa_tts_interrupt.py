##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
call(transferFrom)
transfer(transferTo, {
    "timeout":60,
    "allowSignals":"#",
    "terminator":"*",
    "machineDetection": {
        "introduction": 'Welcome to Tropo! The hosted offering of Tropo, the Scripting API enables you to build communications applications in JavaScript',
        "interruptIntro": True},
    "onConnect": lambda event : say("dtmf input is " + str(event.value.input) + ", and CPA usertype is " + str(event.value.userType)),
    "onSignal": lambda event : log("==========> the call is interrupted")
})
