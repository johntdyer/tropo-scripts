/*
Please refer to the same insturction in the original file transfer_onconnect_recording_7606.php for testing. 
*/

call(transferFrom)
transfer(transferTo, [
	terminator: '#',
	onConnect: { event ->
		startCallRecording("ftp://public:public@10.75.187.217/QA/recordings/transfer.mp3", [format: "audio/mp3"]),
		say("Transfer new destination success")
		stopCallRecording()
	},
	onSuccess: { event ->
		say("Transfer end success, you will hear the recorded file")
		say("ftp://public:public@10.75.187.217/QA/recordings/transfer.mp3")
	}

])
