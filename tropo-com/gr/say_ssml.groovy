def say_as(value, type){
	ssml_start = "<?xml version='1.0'?>"
	ssml_end = "</say-as>"
	ssml = ssml ="<say-as interpret-as='vxml:"+ type + "'>" + value+""
	ssml_audio="<audio src='https://www.tropo.com/static/audio/tropo-rocks.mp3'>" + "</audio>"
	complete_string = ssml_start + ssml_audio + ssml + ssml_end
	log('@@ Say as: ' + complete_string)
	say(complete_string)
}

await(3000)
answer()
say_as('USD51.33','currency')
say_as('20314253','digits')
say_as('2031.435','number')
say_as('4075551212','phone')
say_as('20090226','date')
say_as('0512a','time')


