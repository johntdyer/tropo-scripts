ask("https://www.tropo.com/static/audio/tropo-rocks.mp3", [
	choices: "[4-5 DIGITS]",
	terminator: '#',
	timeout: 15.0,
	mode: "dtmf",
	interdigitTimeout: 5,
	onChoice: {  event ->
		say("On Choice")
	},
	onBadChoice: { event ->
		say("On Bad Choice")
	}

])
