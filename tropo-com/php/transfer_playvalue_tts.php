<?php
/* Transfer - playvalue=tts, sip2sip or sip2pstn or pstn2sip or pstn2pstn
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   'transferFrom' answer the ring first, then while the call transfer to 'transferFrom' - 'transferTo' can hear the speech from playvalue
*/

call($transferFrom, array("timeout" => 89.0));
transfer($transferTo, array(
        /* 
        "playvalue" => "Hello, welcome to tropo system. This a type of test to send text message, it will help user work in a different way, we use more words to avoid text missing problem"
        */
        "playvalue" => "Please wait, tropo is going to transfer you to ".$transferTo,
        "terminator" => "*",
        "timeout" => 89.0
        )
);
?>
