<?php
/* Transfer - postd with in-digit pause (postd=123ppp456)
  'transferFrom' answer the ring first, then 'transferTo' answer the ring from 'transferFrom' - connecting, now 'transferTo' will able to hear dail '123' three soounds then pause for an additional 3 seconds and then dail sound '456'
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

call($transferFrom);
transfer("$transferTo;postd=123ppp456");
?>
