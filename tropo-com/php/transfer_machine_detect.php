<?php
/* Transfer - machine detect
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

call($transferFrom);
transfer($transferTo, array(
	"timeout" => 30.0,
	"machineDetection" => array("introduction"=>"https://www.tropo.com/static/audio/tropo-rocks.mp3", "voice"=>"kate"),
	"onAnswer" => "answerFNC",
	"onTimeout" => "timeoutFNC"
        )
);
function answerFNC($event) {
	say("test for machine detection");
	_log("--------->" . $event->value->userType);
}
function timeoutFNC($event) {
	_log("transfer time out");
}
?>
