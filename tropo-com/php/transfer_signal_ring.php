<?php
/* The script will be interrupted when received http signal request
   https://api.tropo.com/sessions/<session-id>/signals?action=signal&value=*
   track "sessionid" from debug loggs and replace it to <session-id> in http url then trigger this link
   https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

$sessionid = $currentCall->sessionID;
_log("sessionid is". $sessionid->value);
call($transferFrom);
transfer($transferTo, array(
        "allowSignals" => "*",
        "onSignal" => "signalABC"
        )
);
function signalABC($event) {
        say("Received Signals, will be interrrupted.");
}
?>
