<?php
/* Transfer - signal after bridge, the script will be interrupted when received http signal request
   track "sessionid" from debug logs, trigger http url after the bridge - the first say; Call end after signal action triggered, CAN hear the last say from "onSignal"
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   Replace 'sessionid' with the one tracked from debug logs - https://api.tropo.com/1.0/sessions/<session-id>/signals?action=signal&value=tres, change signal to other value such as '#' - then connected call won't end in this way
*/

$sessionid = $currentCall->sessionID;
_log("sessionid is". $sessionid->value);
call($transferFrom);
transfer($transferTo, array(
        "allowSignals" => "tres", 
        "onConnect" => "connectFNC",
        "onSignal" => "signalFNC"
        )
);
function connectFNC($event) {
        say("transfer success.");
}
function signalFCN($event) {
        say("Signal input, transfer will end");
}
?>
