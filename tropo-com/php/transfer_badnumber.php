<?php
/* 1. This test is for transfer - bad number, while the test we can set "transferTo" as an invalid number
   2. Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   3.'transferFrom' answer the ring first, then wait a while
*/
call($transferFrom);
transfer($transferTo, array(
        "onCallFailure" => "callfailFNC"
        )
);
function callfailFNC($event) {
        say("Transfer failure, please check given number or try again later");
}
?>
