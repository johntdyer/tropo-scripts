<?php
/* Transfer - on connect reject, hangup while transfer on connect.
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
  'transferFrom' answer the ring first, after that 'transferTo' answer the call from 'transferFrom', then converse end soon as hangup() triggered soon
*/

call($transferFrom);
transfer($transferTo, array(
	"onConnect" => "connectFNC"
        )
);
function connectFNC($event) {
	hangup();
}
?>
