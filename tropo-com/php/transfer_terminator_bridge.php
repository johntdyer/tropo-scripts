<?php
/* Transfer - test terminator during after bridge
  'transferFrom' answer the ring frist, then 'transferTo' also answered - at this time hear first say("transfer success"), after the say let 'transferFrom' issue terminator, converse will end soon & 'transferFrom' can hear second say("call will end soon...") 
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

call($transferFrom);
transfer($transferTo, array(
        "terminator" => "#",
        "onConnect" => "connectFNC",
        "onSuccess" => "successFNC"
        )
);
function connectFNC($event) {
        say("transfer is on connecting, you could not hear transferer saying now.");
}
function successFNC($event) {
        say("call will end soon as the terminator triggered.");
}
?>