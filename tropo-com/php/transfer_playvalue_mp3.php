<?php
/* Transfer - playvalue=mp3; 
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
  'transferFrom' answer the ring first, then while the call transfer to 'transferFrom' - 'transferTo' can hear the music from playvalue
*/

call($transferFrom,array(
			"timeout" =>120
			));
say(" hello, will start to transfer you now, please wait.");
transfer($transferTo, array(
			"timeout" => 120,
			 "onTimeout" => "timeoutFCN",
        "playvalue" => "https://www.tropo.com/static/audio/tropo-rocks.mp3"
        )
);
function timeoutFCN($event) {
	    say("Sorry, but nobody answered due to time out.");
	        }
?>
