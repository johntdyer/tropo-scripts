<?php
/* Transfer - postd digits
   'transferFrom' answer the ring first, then 'transferTo' answer call from 'transferFrom', 'transferTo' will able to hear the sound of dail '123'  
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

call($transferFrom);
transfer("$transferTo;postd=123");
?>
