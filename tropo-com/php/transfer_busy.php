<?php
/* Transfer - busy, during the test make sure destination - 'transferTo' is on a call or call other number
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   'transferFrom' answer the ring first, then wait a while as 'transferTo' is busy - onBusy() will issued, 'transferFrom' should hear the say 'User is busy now...'
*/

call("$transferFrom"); {
    transfer("$transferTo", array(
	    "onBusy" => "busyFNC"
            )
    );
    function busyFNC($event) {
        say("User is busy now, can you try again later. User is busy now, please try agin later.");
    }
}
?>
