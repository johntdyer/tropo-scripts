<?php
/* Transfer - test terminator during on connect
  'transferFrom' answer the ring first, then 'transferTo' answer the call from 'transferFrom'; During first say("transfer success...) let 'transferFrom' issue terminator - converse end soon & 'transferFrom' can hear second say("call will end soon...")
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

call($transferFrom);
transfer($transferTo, array(
        "terminator" => "#",
        "onConnect" => "connectFNC",
        "onSuccess" => "successFNC"
        )
);
function connectFNC($event) {
        say("transfer success, this will be interrrupted while terminator triggered during the speech, that means during the long speech after terminator input, call will end soon. you will not able to hear last speech after trigger terminator action");
}
function successFNC($event) {
        say("call will end soon as the terminator triggered");
}
?>
