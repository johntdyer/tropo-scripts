<?php
/* Transfer - answerOnMedia, while the call is considered "answered", the onTimeout event will never fire
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   While the test 'transferTo' not answer the call, 'transferFrom' will hear media playing & won't hear say("As answer on media...")
*/

call($transferFrom);
transfer($transferTo, array(
        "timeout" => 15.0,
	"answerOnMedia" => true,
	"onTimeout" => "timeoutFNC"
        )
);
function timeoutFNC($event) {
	say("As answer on media is on, you should not able hear this speech");
}
?>
