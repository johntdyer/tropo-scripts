<?php
/* Transfer - no answer, 'transferFrom' can hear the notice while time out
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

call($transferFrom);
transfer($transferTo, array(
        "timeout" => 15.0,
        "onTimeout" => "timeoutFNC"
        )
);
function timeoutFNC($event) {
        say("Dear customer, no one answer the call now, please try again later.");
}
?>
