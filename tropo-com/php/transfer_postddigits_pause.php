<?php
/* Transfer - postd pause before digits
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
  'transferTo' will hear 234567 ten seconds after connecting with 'transferFrom'. 
*/

call($transferFrom);
transfer("$transferTo;postd=234567;pause=10s");
?>
