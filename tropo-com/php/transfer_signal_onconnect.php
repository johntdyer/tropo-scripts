<?php
/* This script will be interrupted when received http signal request
   https://api.tropo.com/1.0/sessions/<sessionid>/signals?action=signal&value=#
   track "sessionid" from debug logs, trigger http url after transfer on connect. Speech interrupt during signal action triggered, will NOT able to hear last say from "onSignal"
   https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
*/

//$sessionid = $currentCall->sessionID;
//_log("sessionid is". $sessionid->value);
call($transferFrom); 
transfer($transferTo, array(
        "allowSignals" => "tres",
        "onConnect" => "connectFNC",
        "onSignal" => "signalFNC"
        )
);
function connectFNC($event) {
        //say and wait have the same default signal "*"
        say("transfer is on connect, there will 10 seconds music before the bridge.");
        //wait(10000);
        say("ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/www/audio/music.mp3");
}
function signalFNC($event) {
        say("Signal input, transfer will end");
}
?>
