<?php
/* Transfer - terminator, call will end while terminator triggered. (ringing)
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
  'transferFrom' answer the ring frist, then 'transferTo' start ring, at this time let 'transferFrom' issue terminator, call will end soon
*/

call($transferFrom);
transfer($transferTo, array(
	"terminator" => "#"
        )
);
?>
