for (var i =1; i <= 2; i++){
    var ask_tts = "What's your four or five digit pin? Press pound when finished";
    result = ask("Hello, this is an application to test bargein parameter. \
                  Press 1 to test bargein for true, press 2 to test bargein for false.", {
        mode: "dtmf",
        choices: "1,2"
    });
    if (result.value == 1) 
        ask (ask_tts, {
            choices: "[4-5 DIGITS]",
            terminator: '#',
            bargein: true, 
            mode: "dtmf"
        });
    else if (result.value == 2)
        ask (ask_tts, {
            choices: "[4-5 DIGITS]",
            terminator: '#',
            bargein: false, 
            mode: "dtmf"
        });    
    else
        say("You pressed incorrect digit, please try 1 or 2.")
}

