//This test script is to check mono audio in different formats to play and audio mixed TTS with four types in each mono audio format.    

var repeat_times = 4; 
var audio_mp3_url = "ftp://public:public@10.75.187.217/QA/recordings/mono.mp3";
var audio_wav_url = "ftp://public:public@10.75.187.217/QA/recordings/mono.wav";
var audio_au_url = "ftp://public:public@10.75.187.217/QA/recordings/mono-2.au";
var tts_begin = "Hello";
var tts_middle = " Welcome";
var tts_end = " you are so nice";
for (var j=0; j<=repeat_times; j++){
    result = ask("please press 0, 1 or 2", {
        mode: "dtmf",
        choices: "0,1,2"})
    if (result.value == 0){
        for (var i=0; i<=repeat_times; i++){
            result_mixed = ask("please press 0, 1, 2 or 3",{
                mode: "dtmf",
                choices: "0,1,2,3"
            })
            if(result_mixed.value == 0)
                say(audio_mp3_url);
            else if(result_mixed.value == 1)
                say(tts_begin + audio_mp3_url + tts_middle);
            else if(result_mixed.value == 2)
                say(audio_mp3_url + tts_end);
            else if(result_mixed.value == 3)
                say(tts_begin + audio_mp3_url);
            else
               say ('<speak><audio src="ftp://public:public@10.75.187.217/QA/recordings/mono.mp3">You pressed incorrect digit, please try 0, 1, 2 or 3.</audio></speak>');
        }
    }

    else if (result.value == 1){
        for (var i=0; i<=repeat_times; i++){
            result_mixed = ask("please press 0, 1, 2 or 3",{
                mode: "dtmf",
                choices: "0,1,2,3"
            })
            if(result_mixed.value == 0)
                say(audio_wav_url);
            else if(result_mixed.value == 1)
                say(tts_begin + audio_wav_url + tts_middle);
            else if(result_mixed.value == 2)
                say(audio_wav_url + tts_end);
            else if(result_mixed.value == 3)
                say(tts_begin + audio_wav_url);
            else
                say ('<speak><audio src="ftp://public:public@10.75.187.217/QA/recordings/mono.wav">You pressed incorrect digit, please try 0, 1, 2 or 3.</audio></speak>');
        }
    }

    else if (result.value == 2){
        for (var i=0; i<=repeat_times; i++){
            result_mixed = ask("please press 0, 1, 2 or 3",{
                mode: "dtmf",
                choices: "0,1,2,3"
            })
            if(result_mixed.value == 0)
                say(audio_au_url);
            else if(result_mixed.value == 1)
                say(tts_begin + audio_au_url + tts_middle);
            else if(result_mixed.value == 2)
                say(audio_au_url + tts_end);
            else if(result_mixed.value == 3)
                say(tts_begin + audio_au_url);
            else
                say ('<speak><audio src="ftp://public:public@10.75.187.217/QA/recordings/mono-2.au">You pressed incorrect digit, please try 0, 1, 2 or 3.</audio></speak>');
        }
    }

    else
        say ("You pressed incorrect digit, please try 0, 1 or 2.");
}
