/*
 *This test script is to check ssml accented character in default voice.
 *You will hear the word "Welcome" emphasis first, "tropo" emphasis second, no emphasis third.
 */

function say_as(value,type){
    ssml_start="<?xml version='1.0'?><speak>";
    ssml_end="</say-as></speak>";
    ssml ="<say-as interpret-as='vxml:"+ type + "'>" + value+"";
    ssml_emphasis1="<emphasis>" + "Welcome" + "</emphasis>" + "to tropo";
    ssml_emphasis2="Welcome to" + "<emphasis>" + "tropo" + "</emphasis>";
    ssml_emphasis3="Welcome to tropo.";
    ssml_audio1="<audio src='welcome.wav'>" + ssml_emphasis1 + "</audio>";
    ssml_audio2="<audio src='welcome.wav'>" + ssml_emphasis2 + "</audio>";
    ssml_audio3="<audio src='welcome.wav'>" + ssml_emphasis3 + "</audio>";
    complete_string = ssml_start + ssml_audio1 + ssml_audio2 + ssml_audio3 + ssml + ssml_end;
    log('@@ Say as: ' + complete_string);
    say(complete_string);
}
 
wait(3000);
 
say_as('USD51.33','currency');
say_as('20314253','digits');
say_as('2031.435','number');
say_as('4075551212','phone');
say_as('20090226','date');
say_as('0515a','time');
