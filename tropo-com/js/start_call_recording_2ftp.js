/*
 * This test is to validate startCallRecording API to upload recording file to FTP site.
 *
 * You should choice the format you want to record with, 
 * press 1 to choose wav format, 2 to choose mp3 format, and 3 for au format.
 * So you must trigger the app at least 3 times to cover all the formats.
 *
 * Please say something when you hear "Are you a boy or a girl", and when you hear "Now I am playing back what is recored",
 * you are supposed to hear every words that were spoken by you and by Tropo server clearly without speed up / slow down problems.
 *
 */

function ask_format() {
    listen = ask("Press 1 to record your words into wav format, \
                      Press 2 to record as mp3 format, \
                      Press 3 for dot a u", {
        choices:"1,2,3",
        timeout:5.0,
        mode:"dtmf",
        attempts:3
    });
    
    if(listen.value == "1"){
    	format = "audio/wav";
    	say("you choose wav format");
    }else if (listen.value=="2"){
    	format = "audio/mp3";
    	say("you choose mp3 format");
    }else if (listen.value=="3"){
        format = "audio/au"
        say("you chose a u format");
    }else{
    	format = "audio/wav";
    	say("wrong choice, will use the default record format");
    }
    return format;
}

function start_call_recording_now(ftp_url, chose_format) {
    startCallRecording(ftp_url, {format: chose_format});
    wait(2000);
    result = ask("Are you a boy or a girl?", {bargein: false, choices:"boy, girl", timeout:5.0, attempts:3});
    say("stop " + chose_format.replace("audio/", "") + " recording.");
    stopCallRecording();
}

for (i=1; i<=3; ++i) {
    chose_format = ask_format();
    session_id = currentCall.sessionId;
    //ftp_url = "ftps://qa_tropo:qa_tropo@173.39.154.1:990/QA/recordings/" + session_id + chose_format.replace("audio/", ".");
    ftp_url = "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/" + session_id + chose_format.replace("audio/", ".");
    start_call_recording_now(ftp_url, chose_format);
    say("Now playing back what's your recorded file.");
    say(ftp_url);
    wait(3000);
}

