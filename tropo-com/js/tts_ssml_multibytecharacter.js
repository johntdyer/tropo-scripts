/*
*This test script is to check ssml with multibytecharacter tts in default voice.

*You would hear the multibytecharacter tts before each sentence as the format.
*/

function say_as(value,type){
    ssml_start="<?xml version='1.0'?><speak>";
    ssml_end="</say-as></speak>";
    ssml ="<say-as interpret-as='vxml:"+ type + "'>" + value+"";
    ssml_tts="<s>안녕하세요.만아서 반갑습니다.</s>";
    complete_string = ssml_start + ssml_tts + ssml + ssml_end;
    log('@@ Say as: ' + complete_string);
    say(complete_string);
}
 
wait(3000);
 
say_as('USD51.33','currency');
say_as('20314253','digits');
say_as('2031.435','number');
say_as('4075551212','phone');
say_as('20090226','date');
say_as('0515a','time');
