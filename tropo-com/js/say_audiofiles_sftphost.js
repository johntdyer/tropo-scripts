//This test script is to check audio file hosted on sftp, audio mixed TTS with four types to be played and SSML with TTS fallback.   

var repeat_times = 4; 
var audio_url = "sftp://root:svmsung@10.75.187.217/root/troporocks.mp3";
var tts = "Hello from SFTP";
for(var i=0; i<=repeat_times; i++){
    result = ask("please press 0, 1, 2 or 3", {
        mode: "dtmf",
        choices: "0,1,2,3"
        })
    if (result.value == 0)
        say(audio_url);
    else if (result.value == 1)
        say(tts + audio_url + " welcome");
    else if (result.value == 2)
        say(audio_url + " you are so nice");
    else if (result.value == 3)
        say(tts + audio_url);
    else
        say('<speak><audio src="sftp://root:svmsung@10.75.187.217/root/troporocks.mp3">You pressed incorrect digit, please try 0, 1, 2 or 3.</audio></speak>');
    }