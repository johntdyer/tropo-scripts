//This test script is to check audio file with null value and audio mixed TTS with four types to be played.    

var repeat_times = 4; 
var audio_url = "";
var tts = "Hello";
for(var i=0; i<=repeat_times; i++){
    result = ask("please press 0, 1, 2 or 3", {
        mode: "dtmf",
        choices: "0,1,2,3"
        })
    if (result.value == 0)
        say(audio_url);
    else if (result.value == 1)
        say(tts + audio_url + " Welcome");
    else if (result.value == 2)
        say(audio_url + " you are so nice");
    else if (result.value == 3)
        say(tts + audio_url); 
    else
        say('<speak><audio src="">You pressed incorrect digit,please try 0, 1, 2 or 3.</audio></speak>');
    }