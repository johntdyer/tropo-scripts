/*
*This test script is to check ssml with missing audio url in bulit-in voicename 

*You will hear Steven's sound in each sentence and would not hear any music before each sentence.
*/

function say_as(value,type){
    ssml_start="<?xml version='1.0'?><speak>";
    ssml_end="</say-as></voice></speak>";
    ssml ="<say-as interpret-as='vxml:"+ type + "'>" + value+"";
    ssml_voicename="<voice name = 'Malcolm'>";
    ssml_audio="<audio src=''>" + "</audio>";
    complete_string = ssml_start + ssml_voicename  + ssml_audio + ssml + ssml_end;
    log('@@ Say as: ' + complete_string);
    say(complete_string);
}
 
wait(3000);
 
say_as('USD51.33','currency');
say_as('20314253','digits');
say_as('2031.435','number');
say_as('4075551212','phone');
say_as('20090226','date');
say_as('0515a','time');
