# to check parameter bargein
def ask_case
  result = ask "Hello, this is an application to test bargein parameter. \
                Press 1 to test bargein for true, press 2 to test bargein for false.", {
    :mode => "dtmf",
    :choices => "1, 2, 3, 4"
  }
  return result.value
end
ask_tts = "What's your four or five digit pin? Press pound when finished"
for i in 1..4
  case ask_case 
  when '1'
    ask ask_tts, {
        :choices => "[4-5 DIGITS]",
        :terminator => '#',
        :bargein => true, 
        :mode => "dtmf",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
  when '2'
    ask ask_tts, {
        :choices => "[4-5 DIGITS]",
        :terminator => '#',
        :bargein => false, 
        :mode => "dtmf",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
    when '3'
    ask ask_tts, {
        :choices => "[4-5 DIGITS]",
        :terminator => '#',
        :bargein => true, 
        :mode => "speech",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
  when '4'
    ask ask_tts, {
        :choices => "[4-5 DIGITS]",
        :terminator => '#',
        :bargein => true, 
        #:mode => "dtmf",
        :onChoice => lambda { |event|
            say "On Choice"
        },
        :onBadChoice => lambda { |event|
            say "On Bad Choice"
        }
    }
  end
  wait 2000
end