#sanity test script to check ask ASR speech
result = ask("What's your favorite color? Choose from red, blue or green.", {
   "choices":"red, blue, green",
   "mode":"speech"
})
say("You said " + result.value)
