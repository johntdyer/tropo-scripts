# to check parameter minconfidence
ask "What's your favorite color? Choose from red, blue or green.", {
    :choices => "red, blue, green",
    :mode => "speech",
    :minConfidence => 0.5,
    :onChoice => lambda { |event|
        say "On Choice"
    },
    :onBadChoice => lambda { |event|
        say "On Bad Choice"
    }
}