#sanity test script to check ask ASR dtmf
ask "What's your four or five digit pin?  Press pound when finished", {
    #:choices => "[4-5 DIGITS]",
    :choices => "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_dtmf.grxml",
    :terminator => '#',
    :timeout => 15.0,
    :mode => "dtmf",
    :recognizer => "en-gb",
    :onChoice => lambda { |event|
        say "On Choice" + event.value
    },
    :onBadChoice => lambda { |event|
        say "On Bad Choice" + event.value
    },
    :onTimeout => lambda { |event|
        say "On timeout"
    }
}
