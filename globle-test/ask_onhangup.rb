ask "What's your four or five digit pin? Press pound when finished", {
  :choices => "[4-5 DIGITS]",
  :terminator => '#',
  :mode => "dtmf",
  :voice => "susan",
  :onChoice => lambda { |event|
    say "On Choice"
  },
  :onBadChoice => lambda { |event|
    say "On Bad Choice"
  },
  :onHangup => lambda { |event|
    log "On Hangup"
  }

}
