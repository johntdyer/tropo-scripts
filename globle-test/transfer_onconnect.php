<?php
/* Transfer - on connect, 'transferTo' can hear say("Transfer new destination success")
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
   "transferFrom" will get ring first, then 'transferFrom' answer the call, after that 'transferTo' will get the ring -> 'transferTo' answer the call & hear the say of transfer success
*/

call("$transferFrom");
transfer("$transferTo", array(
    "onConnect" => "connectFNC",
    "terminator" => "#"
    )
);
function connectFNC($event) {
    say("Transfer new destiniation success.");
}
?>

