//conference.js
try{
    var caller = currentCall.callerName
    conference_options = {		
	terminator: "*",
	playTones: true,
        /**the two prompt parameters following only apply to tropo 12.1, 12.2**/
        joinPrompt: "Hello " + caller + ", welcome to join tropo conference. Press the star key to quit.",
        leavePrompt: caller + " are leaving the conference.",
        voice:"susan"
    }
    //default try times
    var try_times = 3
    for (var i=1; i<=try_times; i++) {
        response = ask("please input the password.", {
            bargein: true,
            choices:"[4-6 DIGITS]",
            terminator:"#",
            timeout:30.0,
            mode:"dtmf",
            voice:"susan"})
        //the response.value is the dtmf input digits for password(actually it's conference ID)
        if (response.value != "671238") { 
            if (i != try_times)
                say("you pressed incorrect password, try again.", {voice:"susan"})
            else
                say("you have input incorrect password " + i + " times, goodbye!", {voice:"susan"})
        }else{
            conference (response.value, conference_options)
            break
        } 
        wait(1000)
    }
}catch(e) {
    log(e)
    throw (e)
}
