/*for (var i=1; i<=6; i++){
    result = ask("which operation do you want to do? \
                  press 1 for dtmf.grxml, press 2 for voice.grxml, \
                  press 3 for word.grxml, press 4 for dtmf.gram, \
                  press 5 for voice.gram, press 6 for word.gram", {choices: "1,2,3,4,5,6", mode: "dtmf", voice: "susan"})
    if (result.value == 1){
        ask("Please choose any digits, Press pound when finished",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_dtmf.grxml",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you chose" + event.value, {voice:"susan"});
            },
            mode: "dtmf"
        });
    }
    else if (result.value == 2){
        ask("Please speak any digits",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_voice.grxml",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you have spoken" + event.value, {voice:"susan"});
            }
        });
    }
    else if (result.value == 3){
        ask("Please choose your favourite color: red, blue, green, yellow, white or black ",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_word.grxml",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you have chose the color" + event.value, {voice:"susan"});
            }
        });
    }
    else if (result.value == 4){
        ask("Please choose any digits, Press pound when finished",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_dtmf.gram",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you chose" + event.value, {voice:"susan"});
            },
            mode: "dtmf"
        });
    }
    else if (result.value == 5){
        ask("Please speak any digits",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_voice.gram",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you have spoken" + event.value, {voice:"susan"});
            }
        });
    }
    else if (result.value == 6){
        ask("Please choose your favourite color: red, blue, green, yellow, white or black ",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_word.gram",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you have chose the color" + event.value, {voice:"susan"});
            }
        });
    }
    else
        say ("You pressed incorrect digit, please try 1, 2, 3, 4, 5 or 6.", {voice:"susan"});
    wait(2000);
}
*/

ask("Please choose any digits, Press pound when finished",{
            choices: "https://bitbucket.org/voxeolabs/tropo-scripts/raw/master/tropo-com/gram/digits_dtmf.grxml",
            voice: "susan",
            onChoice: function(event) {
                say("Thank you, you chose" + event.value, {voice:"susan"});
            },
            mode: "dtmf"
        });