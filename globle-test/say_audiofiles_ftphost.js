//This test script is to check audio file hosted on ftp, audio mixed TTS with four types to be played and SSML with TTS fallback.    

var repeat_times = 4; 
var audio_url = "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/www/audio/music.mp3";
var tts = "Hello";
for(var i=0; i<=repeat_times; i++){    
    result = ask("please press 0, 1, 2 or 3", {
        mode: "dtmf",
        choices: "0,1,2,3",
        voice:"susan"
        })
    if (result.value == 0)
        say(audio_url, {voice:"susan"});
    else if (result.value == 1)
        say(tts + audio_url + " Welcome", {voice:"susan"});
    else if (result.value == 2)
        say(audio_url + " you are so nice", {voice:"susan"});
    else if (result.value == 3)
        say(tts + audio_url, {voice:"susan"}); 
    else
        say ('<speak><audio src="ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/www/audio/music.mp3">You pressed in correct digit, please try 0, 1, 2 or 3.</audio></speak>', {voice:"susan"});
    }



