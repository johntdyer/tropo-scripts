##https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
call(toDial, {
    "timeout":60,
    "allowSignals":"#",
    "machineDetection": {
        "introduction": 'https://www.tropo.com/static/audio/tropo-rocks.mp3',
        "voice": 'susan'},
    "voice":"susan",
    "onAnswer": lambda event : say("Welcome to Tropo! The hosted offering of Tropo, the Scripting API enables you to build communications applications in JavaScript", {"voice":"susan"}) and log("==========>CPA usertype=" + str(event.value.userType)),
    "onSignal": lambda event : log("==========> the call is interrupted")
})
