for i in 1..3
  ask "What's your four or five digit pin? Press pound when finished", {
    :choices => "[4-5 DIGITS]",
    :terminator => '#',
    :timeout => 10.0,
    :mode => "dtmf",
    :voice => "susan",
    :interdigitTimeout => 1.0,
    :onChoice => lambda { |event|
      say "On Choice"
    },
    :onBadChoice => lambda { |event|
      say "On Bad Choice"
    },
    :onTimeout => lambda { |event|
      say "On Timeout"
    },
    :onEvent => lambda { |event|
      say "On Event"
    }
  }
end
