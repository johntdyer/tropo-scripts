<?php
/* This is a test for transfer method - hangup, 1st case: caller hang up while ringing; 2d case: caller hang up after answer; 3d case: callee hangup after answered the call.
   Compare test case 1,2&3: After hangup 1&2 caller can't hear say speech; for case 3 caller can hear the speech in say.
   Use token to trigger this test in browser - https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&transferFrom="sip or pstn"&transferTo="sip or pstn"
  'transferFrom' is the caller, 'transferTo' is callee
 */

call($transferFrom);
say("Hold on please, will transfer your call to " . $transferTo);
transfer($transferTo, array(
        "onSuccess" => "successFNC"
        )
);
function successFNC($event) {
         say("Your call had transfered success, thanks for choosing us");
}
?>
