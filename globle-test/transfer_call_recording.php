<?php
/* 
 *This test is for transfer - call recording on
 *Use token to trigger the test in browser - https://api.tropo.com/1.0/sessions?action=create&token=<TOKEN>&transferFrom=<sip or pstn>&transferTo=<sip or pstn>
 *'transferFrom' answer the ring, then 'transferTo' answer the ring - say some thing for the recording, after that trigger terminator
 */

call($transferFrom);
say("Start transfer recording");
startCallRecording("ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/transfer.mp3");
transfer($transferTo, array(
     	"terminator" => "*"));
stopCallRecording();
say("stop the recording, will play your recorded file.");
say("ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/transfer.mp3");
wait(2000);
?>
