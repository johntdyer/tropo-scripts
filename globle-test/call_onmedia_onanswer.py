# This test is to check that if set parameter 'answerOnMedia' to true
# and the receive a media from the far end,
# the event 'onAnswer' fires and the event 'onTimeout' will never fire.
#
# Define the number to dial in the URL:
# https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=sip:vinaduan@sip2sip.info
#
# Testing process:
# step1: launch the application using URL in browser;
# step2: check the content of log() from server logs.

call(toDial,{
    "answerOnMedia":True,
    "timeout":20,
    "voice":"susan",
    "onAnswer": lambda event : log("==========On answer!"),
    "onTimeout": lambda event : log("==========On timeout! No answer!")
})
