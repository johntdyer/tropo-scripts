call $phone, {
	:allowSignals => "#",
	:voice => "susan",
	:onAnswer => lambda { |event| 
		session_id = $currentCall.sessionId
		record "hello, this is a testing signal script", {
			:allowSignals => "exit",
			:recordURI => "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/" + session_id + ".wav",
			:voice => "susan",
			:onSignal  => lambda { |event|
				say "got the signal!"
			}
		}
	}
}
