#this script for testing ask speech complete timeout in mode dtmf
call $app, {
    :onAnswer => lambda{ |event| 
        wait 3000 
        say "dtmf:1234", {:voice => "susan"} 
        wait 3000 
        say "dtmf:5", {:voice => "susan"}
    }
}
