ask "What's your four or five digit pin?  Press pound when finished", {
    :choices => "[4-5 DIGITS]",
    :terminator => '#',
    :timeout => 10.0,
    :mode => "dtmf",
    :attempts => 3,
    :voice => "susan",
    :interdigitTimeout => 1.0,
    :onChoice => lambda { |event|
        say "On Choice"
    },
    :onBadChoice => lambda { |event|
        say "On Bad Choice"
    },
    :onTimeout => lambda { |event|
        say "On timeout"
    }

}

