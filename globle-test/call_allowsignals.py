# Test Title: the test is to test signal can or cannot interrupt ringing. 
#
# Precondition: the destination does not answer.
#   Define the number to dial in the URL1:
#   https://api.tropo.com/1.0/sessions?action=create&token=TOKEN&toDial=<sip:vinaduan@sip2sip.info>
#   Define the signal in the URL2:
#   https://api.tropo.com/1.0/sessions/<session-id>/signals?action=signal&value=%23
# 
# Test steps:
#   step1: launch the application using URL1 in browser;
#   step2: assign a matching/unmatched signal using URL2 in browser;
#   step3: check ringing whether interrupted;
#   step4: check the content of log() from server logs.

call(toDial,{
    "allowSignals":"#",
    "onSignal":lambda event: log ("==========Received signals, will be interrupted.")
})