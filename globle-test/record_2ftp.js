/*function ask_format() {
    result = ask("Which format do you want to record? Press 1 for wav, press 2 for mp3, 3 for a u.", {
        choices: "1, 2, 3",
        mode: "dtmf",
        voice: "susan"
    });
    if(result.value == '1')
        format = "audio/wav";
    else if (result.value == '2')
        format = "audio/mp3";
    else if (result.value == '3')
        format = "audio/au";
    else
        format = "audio/wav";
    return format;
}

function record_now(ftp_url, chose_format) {
    record("Tell us what's your favorite color. Press pound key to leave record.", {
        beep: true,
        terminator: "#",
        maxTime: 600,
        recordMethod: "POST",
        recordURI: ftp_url,
        recordFormat: chose_format,
        voice: "susan",
        onRecord: function() {
            wait(3000);
            say("Thanks for your recording, will play back you recorded.", {voice:"susan"});
        }
    });   
}

for (i=1; i<=3; ++i) {
    chose_format = ask_format();
    session_id = currentCall.sessionId;
    //ftp_url = "ftps://qa_tropo:qa_tropo@173.39.154.1:990/QA/recordings/" + session_id + chose_format.replace("audio/", ".");
    ftp_url = "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/" + session_id + chose_format.replace("audio/", ".");
    record_now(ftp_url, chose_format);
    say("Your will hear your recorded file," + ftp_url, {voice:"susan"});
}
say("End recording, goodbye!", {voice:"susan"}); */

var today = new Date();
var ftp_url = "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/" + today.getMinutes() + "_" + today.getSeconds() + ".mp3";

say("Welcome to recording test!", {voice:"susan"});
record("Tell us how you feel?", {
	 	recordURI:ftp_url,
	 	bargein:false,
	 	timeout:5.0,
	 	voice:"susan",
	 	onTimeout: function(event) {
	 		say("On Time Out");
	 	}
	 });
wait(1000);
say("will play the recorded audio file.", {voice:"susan"});
wait(2000);
say(ftp_url, {voice:"susan"});
say("End recording, goodbye!", {voice:"susan"});
