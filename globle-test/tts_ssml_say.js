/*
*This test script is to check ssml with default voice

*You would hear each sentence as the format.
*/

function say_as(value,type){
    ssml_start="<?xml version='1.0'?><speak>";
    ssml_end="</say-as></speak>";
    ssml ="<say-as interpret-as='vxml:"+ type + "'>" + value+"";
    ssml_audio="<audio src='https://www.tropo.com/static/audio/tropo-rocks.mp3'>" + "</audio>";
    complete_string = ssml_start + ssml_audio + ssml + ssml_end;
    log('@@ Say as: ' + complete_string);
    say(complete_string, {voice:"susan"});
}
 
wait(3000);
answer(); 
say_as('USD51.33','currency');
say_as('20314253','digits');
say_as('2031.435','number');
say_as('4075551212','phone');
say_as('20090226','date');
say_as('0512a','time');

/*
wait(3000);

say ("<?xml version='1.0' encoding='UTF-8'?><speak><prosody volume='soft'> Welcome to Tropo! This volume is soft. </prosody></speak>");

say ("<?xml version='1.0' encoding='UTF-8'?><speak><prosody volume='x-loud'> Welcome to Tropo! This volume is x-loud. </prosody></speak>");

say ("<?xml version='1.0' encoding='UTF-8'?><speak><prosody volume=x-loud> Welcome to Tropo! This volume is x-loud. </prosody></speak>");
*/
