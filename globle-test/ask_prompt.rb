def ask_case
  result = ask "Which case do you want to test, press 1 or 2 for empty string, 3 for invalid url, 4 for valid url?", {
    :mode => "dtmf",
    :choices => "1, 2, 3, 4",
    :voice => "susan"
  }
  return result.value
end
options = {
  :choices => "[4-5 DIGITS]",
  :terminator => '#',
  :timeout => 15.0,
  :mode => "dtmf",
  :voice => "susan",
  :interdigitTimeout => 1.0,
  :onChoice => lambda { |event|
    say "On Choice"
  },
  :onBadChoice => lambda { |event|
    say "On Bad Choice"
  }
}
for i in 1..4
  case ask_case 
  when '1'
    prompt = ""
  when '2'
    prompt = " "
  when '3'
    prompt = "http://www.noexsitsite.com/norocks.mp3"
  when '4'
    prompt = "https://www.tropo.com/static/audio/tropo-rocks.mp3"
  end
  ask prompt, options
  wait 2000
end
