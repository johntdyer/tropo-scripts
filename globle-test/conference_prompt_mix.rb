#conference_prompt.rb
conferenceID = $conference 
participant_list = [] 
participant_list = $participants.split(",") 
threads = [] 
participant_list.each do |x| 
  threads << Thread.new do

    if x.nil?
      log "#"*10 + " no phone number given."
    else
      log "#"*10 + " #{x} join in."
    end

    conferenceOptions = {
      :terminator   => "*",
      :voice=>"susan",
      :joinPrompt   => {"value" => "https://ccrma.stanford.edu/~jos/mp3/gtr-jazz.mp3 someone joined"},
      :leavePrompt  => {"value" => "someone has left https://ccrma.stanford.edu/~jos/mp3/gtr-jazz.mp3"}
    }
    
    call x , {
      :timeout=>90,
      :voice=>"susan",
      :onAnswer => lambda { |event|
        newCall = event.value 
        newCall.conference conferenceID, conferenceOptions
        newCall.hangup
      }
    }
  end
end
 
threads.each { |t| t.join }