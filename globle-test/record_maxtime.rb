session_id = $currentCall.sessionId
ftp_url = "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/" + session_id + ".mp3"
record "how you feel!", {
  :recordURI => ftp_url,
  :maxTime => 10, 
  :voice => "susan",
  :onEvent => lambda { |event|
    log "Exceed the maxTime."
  }
}
