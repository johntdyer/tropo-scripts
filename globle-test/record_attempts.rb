session_id = $currentCall.sessionId
ftp_url = "ftp://voxeolabsqa:voxeolabs9a@ftp.tropo.com/recordings/" + session_id + ".mp3"
record "Tell us how you feel!", {
    :recordURI => ftp_url,
    :attempts => 3,
    :voice => "susan"
    :timeout => 5
}
wait 1000
say "will play the recorded audio file.", {:voice => "susan"}
wait 2000
say ftp_url, {:voice => "susan"}
