result = ask("What's your favorite color? Choose from red, blue or green.", {
   "choices":"red, blue, green",
   "mode":"speech",
   "voice":"susan"
})
say("You said " + result.value)
