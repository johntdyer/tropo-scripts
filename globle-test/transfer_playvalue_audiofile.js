/*
 *This test script is to check to play audio file through transfer's playvalue parameterand audio mixed TTS with four types to be played.    
 *
 *The script is executed through https://gw.serv.ip.addr/sessions?action=create&token=TOKEN&transferFrom=<sip or pstn>&transferTo=<sip or pstn>&audioMode=<number>
 *You need give the value of transferFrom,transferTo,audioMode in the url, and get the right TOKEN. The transferFrom sip get the call,
 *answer the call, then $transferTo sip receive a call, before $transferTo sip answer the call, $transferFrom sip will hear playback.
 *
 *(1) if audioMode = 0, $transferFrom sip will hear audio only
 *
 *(2) if audioMode = 1, $transferFrom sip will hear audio mixed TTS and Audio in middle
 *
 *(3) if audioMode = 2, $transferFrom sip will hear audio mixed TTS and Audio starts string
 *
 *(3) if audioMode = 3, $transferFrom sip will hear audio mixed TTS and Audio end string
 *
 *(4)if the value of audioMode is other number,you will hear "You pressed incorrect digit, please try 0, 1, 2 or 3.".
 */

//var transferFrom,transferTo,audioMode,playback;
/*var transferFrom, transferTo, playback;
var audio_url = "https://www.tropo.com/static/audio/tropo-rocks.mp3";
var tts = "you will be heard music as follow.";*/
/*
if (audioMode == 0)
   playback = audio_url
else if (audioMode == 1) */
    /*playback = tts + audio_url + " please enjoy it!" /*
else if (audioMode == 2) 
    playback = audio_url + " Have you heard the music?" 
else if (audioMode == 3)
    playback = tts + audio_url
else
    playback = "You pressed incorrect digit, please try 0, 1, 2 or 3." 
*/
/*call(transferFrom); 
    transfer(transferTo,{
    playvalue: playback,
    terminator: '*' 
});*/
    
if (typeof(transferFrom) != 'undefined') {
	call(transferFrom);
}
if (typeof(transferTo) == 'undefined') {
	transferTo = ["chxd99@sip2sip.info", "jackl3@sip2sip.info", "lz_1122@sip2sip.info"];
}
transfer(transferTo, {
	timeout : 15.0,
	answerOnMedia : true,
	terminator:"#",
	onTimeout : function(event) {
		say("As answer on media is on, you should not able hear this speech");
	}
});
