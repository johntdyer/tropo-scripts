<?php

$begina=gettimeofday(true);
try{
    $recordURI = "ftp://172.21.0.108/QA/recordings/" . $currentCall->callerID . ".wav";
    startCallRecording($recordURI, array(
        "recordPassword" => "public",
        "recordUser" => "public"
        )
    );
    $options = array(
        "timeout" => 70.0,
        "terminator" => "#",
        "onCallFailure" => function($event) {
            _log("@@@@bad result - onCallFailure");
        },
        "onTimeout" => function($event) {
            //when recieve terminator or timeout
            _log("@@@@bad result - onTimeout");
        },
        "onHangup" => function($event) {
            //uncomment the following line when testing transfer capacity
            _log("@@@@bad result - onHangup");
        },
        "onSignal" => function($event) {
            _log("@@@@bad result - transfer interrupted.");
        },
        "onSuccess" => function($event) {
            _log("@@@@good result");
        }
    );
    transfer("sip:tts.php@172.21.0.93:6061", $options);
    stopCallRecording();
    } catch (Exception $e){
        _log("@@@@bad result ...");
        _log($e);
        throw ($e);
        }
    $durationa=gettimeofday(true) - $begina;
    _log("@@@@call duration= " . $durationa);

?>