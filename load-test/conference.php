<?php
    $begina=gettimeofday(true);
    try {
        answer();
        $response = ask("dtmf:12345", array(
        "choices" => "[4-6 DIGITS]",
        "terminator" => "#",
        "timeout" => 15.0,
        "mode" => "dtmf"
            )
        );
        if ($response->name=='choice') {
            //the response.value is the dtmf input digits for conference ID
            conference ($response->value, array(
                "terminator" => "*",
                "playTones" => true,
                "onChoice" => function ($event) {
                    _log("@@@@good result -  Received Terminator");
                 },
                 "onHangup" => function ($event) {
                    _log("@@@@bad result - User hangup w/o using terminator");
                 })
            );
        }else{
            _log ("@@@@invalid conference ID: " . $response->value);
        }
    //hangup()
    } catch (Exception $e) {
        _log("@@@@bad result ...");
        _log($e);
        throw ($e);
    }
    $durationa=gettimeofday(true) - $begina;
    _log("@@@@call duration= " . $durationa);

?>