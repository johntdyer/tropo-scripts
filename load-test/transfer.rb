#transfer.rb
begina = Time.now
begin
#	answer
#	recordURI = "ftp://test8.pek.voxeo.com/QA/recordings/" + $currentCall.callerID + ".wav"
#	startCallRecording recordURI, { :recordPassword => "public", :recordUser => "public" }
	options = { 
        	:terminator => "*",
            	:timeout => 70.0,
		:onCallFailure => lambda { |event|
			log "@@@@bad result - onCallFailure"
		},
		:onTimeout => lambda { |event| 
			#when recieve terminator or timeout
			log "@@@@bad result - onTimeout"	
		},
		:onHangup => lambda { |event| 
                        #uncomment the following line when testing transfer capacity
			log "@@@@bad result - onHangup"
		},
		:onSignal => lambda { |event|
			log "@@@@bad result - transfer interrupted."
		},
		:onSuccess => lambda { |event|
			log "@@@@good result"
		}			
	}
	transfer "sip:tts_as_transferee.js@172.21.0.93:6061", options
#	stopCallRecording
rescue Exception => e
#	log "@@@@bad result - " + e.message
#	raise e.message
	log "@@@@bad result ..."
	log e
	throw e
end
durationa = ((Time.now - begina) * 1000.0).to_i
log "@@@@call duration=" + durationa.to_s
