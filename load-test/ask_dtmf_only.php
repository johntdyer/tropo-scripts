<?php

$begina=gettimeofday(true);
try{
	answer();
	$options = array ( 
		"choices" => "[10-15 DIGITS]",
        "terminator" => '#',
        "timeout" => 30.0,
        "mode" => "dtmf",
        "interdigitTimeout" => 20
	);
    for($i = 1; $i <= 15; ++$i){
		$result = ask("dtmf:12345", $options);
		if ($result->name == 'choice') {
			if ($result->value == "1234567890") {
				_log ("@@@@good result @" . $i . " times.");
			}
			else {
				_log("@@@@bad result [" . $result->value . "] @" . $i . " times.");
			}
		}
		else {
			_log("@@@@bad result [" . $result->name . "] happened @" . $i . " times.");
		}
		wait (1000);
	}
	hangup();
} catch (Exception $e){
        _log("@@@@bad result ...");
        _log($e);
        throw ($e);
        }
    $durationa=gettimeofday(true) - $begina;
    _log("@@@@call duration=" . $durationa);

?>