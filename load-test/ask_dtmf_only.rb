#ask_dtmf_only.rb
begina = Time.now
begin
	answer
	options = { :choices => "[10 DIGITS]",
		    :terminator => '#',
		    :mode => "dtmf",
		    :interdigitTimeout => 20}
	for i in 1..15
		result = ask "dtmf:12345", options
		if result.name == 'choice'
			if result.value == "1234567890"
				log "@@@@good result @" + i.to_s + " times."
			else
				log "@@@@bad result ["+result.value+"] @" + i.to_s + " times."
			end
		else
			log "@@@@bad result ["+result.name+"] happened @" + i.to_s + " times."
		end
		wait 1000
	end
	hangup
rescue Exception => e
#	log "@@@@bad result - " + e.message
#	raise e.message
	log("@@@@bad result ...")
	log(e)
	throw (e)
end
durationa = ((Time.now - begina) * 1000.0).to_i
log "@@@@call duration=" + durationa.to_s
