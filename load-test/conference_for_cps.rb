calls = 600
num = calls / 30
conferenceID = 1 + rand(num)

log "**********************Conference ID is: " + conferenceID.to_s
say "hello"
threads = []
threads << Thread.new do
        conference(conferenceID)
end

threads << Thread.new do
        # Call the number (x) and setup onAnswer event for further processing
        call "sip:xxxxxx@xxxxxxxxxxxxxx", {
                :onAnswer => lambda { |event2|

                                newCall2 = event2.value
                                newCall2.say "hello"
                                # Inform the person being called that this is a conference call and ask them to join
                                newCall2.conference(conferenceID)
                }
        }
 end

 # wait 100 seconds then hangup to exit conference for both inbound call and outbound call
 wait 100000
 hangup
 log "######################hangup"
 
