<?php

$begina=gettimeofday(true);
try {
    $options = array(
        "onTimeout" => function($event) {
            _log("@@@@bad result - onTimeout");
        },
        "onBadChoice" => function($event) {
            _log("@@@@bad result - onBadChoice");
        },
        "onSilenceTimeout" => function($event) {
            _log("@@@@bad result - onSilenceTimeout");
        },
        "onHangup" => function($event) {
            
            //uncomment the following line for transfer capacity test
            _log("@@@@bad result - Hangup by remote");

            //comment follwing line for transfer CPS test
            //_log("@@@@good result - Hangup by remote");
        }
    );
    $result = say ("Welcome to Tropo, " . "This is description for say method, " . 
                   "Says something to the user, " . 
                   "Unlike ask, this function has no ability to wait for a response from the user, " . 
                   "On voice or phone sessions: Say can play text strings using Text To Speech or "  .
                   "play URLs as audio files, Since Tropo is synchronous, say is a blocking method, " .
                   "This means no other method can run until say is complete, In the case of a voice session, " .
                   "this can either be the text to be rendered by the Text to Speech Engine, " .
                   "or a URL to an audio file to be played, In the case of a text messaging session, " .
                   "Says something to the user, " .
                   "Unlike ask, this function has no ability to wait for a response from the user, " .
                   "On voice or phone sessions: Say can play text strings using Text To Speech or "  .
                   "play URLs as audio files, Since Tropo is synchronous, say is a blocking method, " .
                   "This means no other method can run until say is complete, In the case of a voice session, " .
                   "this can either be the text to be rendered by the Text to Speech Engine, " .
                   "or a URL to an audio file to be played, In the case of a text messaging session, " .
                   "this will be the text to be sent to the user, Thank you, see you next time", $options);
    
    //uncomment these line for transfer capacity test 
  /*  
    if($result->name == 'choice'){
        _log("@@@@good result");
    }
    else {
        //do nothing, error has already logged in onXXX methods
    }
   */ 
} catch (Exception $e){
        _log("@@@@bad result ...");
        _log($e);
        throw ($e);
        }
    $durationa=gettimeofday(true) - $begina;
    _log("@@@@call duration=" . $durationa);

?>